var express = require('express');
var router = express.Router();
var tcpServer = require('../lib/tcp_endpoint')
var httpServer = require('../lib/http_endpoint');

const page_title = 'Admin Server'
const getserverStatus = () => {
  return {
    tcp: tcpServer.serverStatus(),
    http: httpServer.serverStatus()
  }
}


router.get('/', function(req, res, next) {
  if (req.user){
    res.render('index', { title: page_title, status: getserverStatus() });
  }else{
    res.redirect("/login")
  }
});


/* GET home page. */
router.get('/dashboard', function(req, res, next) {
  if (req.user){
    res.render('index', { title: page_title, status: getserverStatus() });
  }else{
    res.redirect("/login")
  }
});

router.get('/start/:servername', async function(req, res, next) {
  let servername = req.params.servername
  servername == 'httpendpoint' ? await httpServer.startServer() : tcpServer.startServer()
  res.redirect('/')
});

router.get('/stop/:servername', async function(req, res, next) {
  let servername = req.params.servername
  servername == 'httpendpoint' ? await httpServer.stopServer() : await tcpServer.stopServer();
  res.redirect('/')
});

module.exports = router;
