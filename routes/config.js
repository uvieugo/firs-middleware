var express = require('express');
var router = express.Router();
const {getConfig, updateConfig} = require("../lib/connection/db");

router.get('/', async function(req, res, next) {
  if(req.user){
    var configData = await getConfig();
    // console.log(configData)
    res.render('config', {configData})
  }else{
    res.render('login')
  }
});


router.post('/save', async function(req, res, next) {
  if(req.user){
    var configData = await updateConfig(req.body);
    res.redirect('/config')
  }else{
    res.render('login')
  }
});

module.exports = router;
