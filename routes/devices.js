var express = require('express');
var router = express.Router();
const {getAllDeviceMap, updateDeviceMap, insertDeviceMap, getDeviceMap} = require("../lib/connection/db");

const page_title = 'Configure Devices'


/* GET Devices page. */
router.get('/', async function(req, res, next) {
  if(req.user){
    let mappings = await getAllDeviceMap()
    res.render('devices', { title: page_title , devices: mappings});
  }else{
    res.redirect("/login")
  }
});

router.post('/add', async function(req, res, next) {
  console.log(req.body)
  let {workstation_id, business_device, device_type} = req.body
  // console.log(workstation_id)
  let result = await insertDeviceMap(workstation_id,business_device,device_type)
  // .catch( error => {
  //   console.log(error)
  // })
  // console.log(result)
  res.redirect('/devices')
  // res.render('devices', { title: page_title });
});


module.exports = router;
