var express = require('express');
const fs = require("fs");
const {stringify} = require('csv');
var router = express.Router();
const {viewMessages} = require("../lib/connection/db");

const page_title = 'Export Data'

router.get('/', async function(req, res, next) {
  // if (req.user){
    res.render('reports', { title: page_title});
  // }else{
  //   res.redirect("/login")
  // }
});

router.post('/view', async function(req, res, next) {
  // console.log(req.body)
  let {source,startDate,endDate} = req.body
  let result = await viewMessages(source,startDate,endDate)

  stringify(result, {header:true}, function (err, output) {
    let filename = `./tmp/data${Date.now()}.csv`
    fs.writeFile(filename, output, function (err) {
      if (err) throw err
      console.log("Export Complete")
      res.download(filename)
    });
  })

});


module.exports = router;
