const {insertMessage, getConfig, getMessage, updateMessage} = require("../connection/db");
const {postToFirs} = require('../firs/firs_post');
const {generateFirsMessage} = require('../firs/firs_message');
const moment = require('moment');

const voidBill = async (rowID) => {
  console.log("---Start Void Process----")
  // console.log(process.env.NODE_ENV)  
  if (rowID!=null) {
    //void row
    let rowData = await getMessage(rowID)
    await updateMessage(rowID,{void_status: "VOIDED"})

    // create new message from row that was voided
    let [newRowID,from_pms_message, bill_number] = await parsePMSMessage(JSON.parse(rowData.fiscal_message),rowID)
    from_pms_message.bill_number = newRowID

    let firsMessage = await generateFirsMessage(from_pms_message, 'PMS')
    updateMessage(newRowID,{message_to_firs:JSON.stringify(firsMessage)})

    let voidResponse = await postToFirs(firsMessage).then((response) => {
      updateMessage(newRowID,{firs_response: response.payment_code, response_code: response.status_code, success: true})
      // console.log(response)
      return new Promise (resolve => {
        resolve(response)
      })

    });

    console.log("---End Void Process----")
  }
}

async function parsePMSMessage(ctx,rowID){
  // console.log(ctx)
  let {pms_business_device, pms_vat_tax_string, pms_state_tax_string, pms_svc_tax_string, vat_tax_rate, svc_tax_rate, state_tax_rate} = await getConfig()
  pms_state_tax_string = pms_state_tax_string.split(",")
  pms_svc_tax_string = pms_svc_tax_string.split(",")
  pms_vat_tax_string = pms_vat_tax_string.split(",")
  let reducer = (a, c) => a + c
  let business_device = pms_business_device
  let bill_number = ctx.DocumentInfo.BillNo
  let bill_datetime = ctx.DocumentInfo.BusinessDateTime

  let trxCodes = (process.env.FLIP_VAT_CODES + process.env.FLIP_SVC_CODES + process.env.FLIP_STATE_CODES).split(",")
  let vatTrxCodes = process.env.FLIP_VAT_CODES.split(",")
  let svcTrxCodes = process.env.FLIP_SVC_CODES.split(",")
  let stateTrxCodes = process.env.FLIP_STATE_CODES.split(",")
  let payment_type = "0"
  let total_info
  let postingTotal
  let netPostingTotal
  let vatPostingTotal
  let svcPostingTotal
  let statePostingTotal

  total_info = ctx.FolioInfo.TotalInfo
  if(total_info){
    postingTotal = total_info.hasOwnProperty("GrossAmount") ?  total_info.GrossAmount : 0
    netPostingTotal = total_info.hasOwnProperty("NetAmount") ?  total_info.NetAmount : 0
    vatPostingTotal =  total_info.Taxes.hasOwnProperty("Tax") ? total_info.Taxes.Tax.filter(obj => pms_vat_tax_string.includes(obj.Name)).map(obj => obj.Value).reduce(reducer,0.0) : 0
    svcPostingTotal =  total_info.Taxes.hasOwnProperty("Tax") ? total_info.Taxes.Tax.filter(obj => pms_svc_tax_string.includes(obj.Name)).map(obj => obj.Value).reduce(reducer,0.0) : 0
    statePostingTotal = total_info.Taxes.hasOwnProperty("Tax") ? total_info.Taxes.Tax.filter(obj => pms_state_tax_string.includes(obj.Name)).map(obj => obj.Value).reduce(reducer,0.0) : 0
  }
  else{
    postingTotal = ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").map(obj => obj.GrossAmount).filter(obj => obj ).reduce(reducer,0.0)
    netPostingTotal = ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => !trxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
    vatPostingTotal =  ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => vatTrxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
    svcPostingTotal =  ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => svcTrxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
    statePostingTotal = ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => stateTrxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
  }

  console.log(`Posting Total: ${postingTotal}, Net Total: ${netPostingTotal}, VAT Total: ${vatPostingTotal}. SVC Total: ${svcPostingTotal}, State Total: ${statePostingTotal}`)


  function setBillDateTime(){
    return (process.env.NODE_ENV != 'development' ? moment().format().slice(0, -6) : bill_datetime)
  }

  let from_pms_message = {
    "bill_number": bill_number,
    "fiscal_folio_id": bill_number,
    "bill_datetime": setBillDateTime(),
    "payment_type": payment_type,
    "total_value": "-"+parseFloat(postingTotal).toFixed(2),
    "tax_free": "0",
    "base_value": "-"+parseFloat(netPostingTotal).toFixed(2),
    "business_device": business_device,
    "bill_taxes": [{
      "rate": vat_tax_rate,
      "base_value": `-${parseFloat(netPostingTotal).toFixed(2)}`,
      "value": `${(vatPostingTotal > 0 ? "-"+parseFloat(vatPostingTotal).toFixed(2) : "0.00")}`
    }],

    "bill_tax_gst": [{
      "rate": state_tax_rate,
      "base_value": `-${parseFloat(netPostingTotal).toFixed(2)}`,
      "value": `${(statePostingTotal > 0 ? "-"+parseFloat(statePostingTotal).toFixed(2) : "0.00")}`
    }],

    "bill_tax_other": [{
      "tax_name": "Service Charge",
      "rate": svc_tax_rate,
      "base_value": `-${parseFloat(netPostingTotal).toFixed(2)}`,
      "value": `${(svcPostingTotal > 0 ? "-"+parseFloat(svcPostingTotal).toFixed(2) : "0.00")}`
    }]
  }

  let dbMessage = { fiscal_message: ctx, source: "PMS", system_bill_number: bill_number, void_status: `VOIDED-${rowID}`}
  let newrowID = await insertMessage(dbMessage);
  
  return [newrowID, from_pms_message, bill_number];

}
exports.voidBill = voidBill