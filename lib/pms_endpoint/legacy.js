const needle = require('needle');
const moment = require('moment');
const {XMLParser, XMLBuilder, XMLValidator} = require('fast-xml-parser');
const {generateFirsMessage} = require('../firs/firs_message');
const {generateLegacyResponse} = require('./legacy_response');
const {insertMessage, getConfig, updateMessage, getBillNumber, getMessage} = require("../connection/db");
const {postToFirs} = require('../firs/firs_post');

const parseLegacy = async (data) => {
  let {pms_pos_rooms} = await getConfig()
  pms_pos_rooms = pms_pos_rooms.split(",")
  let {payload_url, response_params, jndiname, response_url} = data

  if (payload_url.indexOf('https') === 0) {
    payload_url = payload_url.replace('https', 'http')
  }

  if (response_url.indexOf('https') === 0) {
    response_url = response_url.replace('https', 'http')
  }

  // get fiscal data from pms
  let xml_data = await getPayload(payload_url)
  // console.log(xml_data)
  // end get fiscal data from pms

  // check bill number from fiscal data
  let bill_number = xml_data.head.document_info.BILLNUMBER.toString()

  // check if this bill number exists in the db( check for duplicates)
  let prevMessageId = await getBillNumber(bill_number,"PMS")
  let voidProcess
  // if row with bill number exist start and wait for void process
  if (prevMessageId !=null){
    // void previous bill if bill number exists
    voidProcess = await voidBill(prevMessageId)
  }

  // insert message into database
  let rowID = await insertMessage({fiscal_message: xml_data, source: "PMS", system_bill_number: bill_number});
  
  let post_data = await parseXml(xml_data)

  let folio_number = post_data.bill_number
  let room_number = post_data.hasOwnProperty('room_number') ? post_data.room_number : '0';
  let no_post_response = { 
    FiscalFolioNo: folio_number,
    returnCode: "not sent"
  }

  // // post response to pms
  if (pms_pos_rooms.includes(room_number)) {
    getCallback("",false,response_params,response_url,jndiname)
  } else {
    
    let folio_number = post_data.bill_number
    
    post_data.bill_number = rowID

    let firsMessage = await generateFirsMessage(post_data,'LEGACYPMS')

    updateMessage(rowID,{message_to_firs:JSON.stringify(firsMessage)})
  
    let response = await postToFirs(firsMessage).then((response) => {
      //Parse response into require PMS format
      return generateLegacyResponse(folio_number, response, rowID);
    });

    let command = xml_data.command
    let xmlResponse = makeResponse(command, response)

    getCallback(xmlResponse,response,response_params,response_url,jndiname)
    // Generate and send response back to PMS
    // return response;
  }
}

const getPayload = async (payload_url) => {
  let {mask_guest_info} = await getConfig()
  // console.log(mask_guest_info)
  var options = {
    rejectUnauthorized : false,
    parse_response: false
  }
  return new Promise(resolve => {
    let xmlDataStr
    needle.get(payload_url, options, (err, resp) => {
      if(err){
        console.log(err)
      }else{
        // console.log(resp.body)
        xmlDataStr = resp.body
        const options = {
          ignoreAttributes: true,
          attributeNamePrefix : "@_"
        };
        const parser = new XMLParser(options);
        let output = parser.parse(xmlDataStr);
        let xml_data = output.folio
        if (mask_guest_info){
          let headData = xml_data.head
          headData.payee["PAYEEFIRSTNAME"] = "xxxxx"
          headData.payee["PAYEELASTNAME"]  = "xxxxx"
          headData.payee["PAYEESALUTATION"]  = "xxxxx"
          headData.payee["NATIONALITY"] = "xxxxx"
          headData.payee_address["ADDRESSEEADDRESS1"] = "xxxxx"
          headData.payee_address["ADDRESSEEADDRESS2"] = "xxxxx"
          headData.payee_address["ADDRESSEECITY"] = "xxxxx"
          headData.payee_address["ADDRESSEECOUNTRY"] = "xxxxx"
          headData.payee_address["ADDRESSEESTATE"] = "xxxxx"
          headData.payee_phone["PAYEEPHONENUMBER"] = "xxxxx"
          headData.guest_info["FIRSTNAME"] = "xxxxx"
          headData.guest_info["LASTNAME"] = "xxxxx"
        }
        resolve(xml_data)
      }
    });

  });

}

const parseXml = async (xml_data) => {
  let {vat_tax_rate, svc_tax_rate, state_tax_rate, pms_state_tax_string, pms_svc_tax_string, pms_vat_tax_string} = await getConfig()
  pms_state_tax_string = pms_state_tax_string.split(",")
  pms_svc_tax_string = pms_svc_tax_string.split(",")
  pms_vat_tax_string = pms_vat_tax_string.split(",")

  let reducer = (a, c) => a + c
  let headData = xml_data.head
  let bodyData = xml_data.body
  let documentInfo = headData.document_info
  let generalInfo = headData.general
  let roomNumber = generalInfo.ROOMNUMBER.toString()
  let bill_number = documentInfo.BILLNUMBER.toString()
  let total_info = bodyData.total_info

  let getTaxValues = (tax_type_array) => {
    // console.log(tax_type_array)
    let tax_array = tax_type_array.map( obj => "TAX"+obj+"AMOUNT" )
    if (tax_type_array.includes("NA")){
      return "0.00"
    }else{
      return tax_array.map( obj => total_info.hasOwnProperty(obj) ? total_info[obj] : "0.0"  )
      .map(obj => typeof obj == 'number' ? obj : parseFloat(obj.replace(/,/g, ''))).reduce(reducer,0.0).toFixed(2)
      
    }
  }

  // let payment_info = bodyData.children.find(x => x.name == 'payment') == undefined ? "O" : bodyData.children.find(x => x.name == 'payment').children.find(x => x.name=="CODE").value
  // let payment_info = typeof bodyData.payment == 'object' ? bodyData.payment[0].CODE : "0"
  let total_value = total_info.TOTALGROSSAMOUNT.toString().replace(/,/g, '')
  let base_value = total_info.TOTALNETAMOUNT.toString().replace(/,/g, '')
  let vat_value = getTaxValues(pms_vat_tax_string)
  let svc_value = getTaxValues(pms_svc_tax_string)
  let state_value = getTaxValues(pms_state_tax_string)
  let final_result

  final_result = {
    "bill_number": bill_number,
    "bill_datetime": moment().format().slice(0, -6),
    "total_value": total_value,
    "net_value": base_value,
    "bill_taxes": [{
      "rate": vat_tax_rate,
      "base_value": base_value,
      "value": vat_value,
    }],

    "bill_tax_gst": [{
      "rate": state_tax_rate,
      "base_value": base_value,
      "value": state_value
    }],

    "bill_tax_other": [{
      "tax_name": "Service Charge",
      "rate": svc_tax_rate,
      "base_value": base_value,
      "value": svc_value
    }],
  }

  if (roomNumber != ""){
    final_result.room_number = roomNumber
  }
  return final_result
}


const voidBill = async (rowID) => {
  console.log("---Start Void Process----")
  
    //void row
    let rowData = await getMessage(rowID)
    // console.log(rowData)
    await updateMessage(rowID,{void_status: "VOIDED"})

    // create new message from row that was voided
    let [newRowID,from_pms_message, bill_number] = await parseDuplicateMessage(JSON.parse(rowData.fiscal_message),rowID)
    from_pms_message.bill_number = newRowID

    let firsMessage = await generateFirsMessage(from_pms_message, 'LEGACYPMS')

    updateMessage(newRowID,{message_to_firs:JSON.stringify(firsMessage)})

    let voidResponse = await postToFirs(firsMessage).then((response) => {
      updateMessage(newRowID,{firs_response: response.payment_code, response_code: response.status_code, success: true})
      // console.log(response)
      return new Promise (resolve => {
        resolve(response)
      })

    });

    console.log("---End Void Process----")
  
}

async function parseDuplicateMessage(message,rowID){
  let {vat_tax_rate, svc_tax_rate, state_tax_rate, pms_state_tax_string, pms_svc_tax_string, pms_vat_tax_string} = await getConfig()
  pms_state_tax_string = pms_state_tax_string.split(",")
  pms_svc_tax_string = pms_svc_tax_string.split(",")
  pms_vat_tax_string = pms_vat_tax_string.split(",")
  // console.log(message)
  let reducer = (a, c) => a + c
  let headData = message.head
  let bodyData = message.body
  let documentInfo = headData.document_info
  let generalInfo = headData.general
  let roomNumber = generalInfo.ROOMNUMBER.toString()
  let bill_number = documentInfo.BILLNUMBER.toString()
  let total_info = bodyData.total_info

  let getTaxValues = (tax_type_array) => {
    // console.log(tax_type_array)
    let tax_array = tax_type_array.map( obj => "TAX"+obj+"AMOUNT" )
    if (tax_type_array.includes("NA")){
      return "0.00"
    }else{
      return tax_array.map( obj => total_info.hasOwnProperty(obj) ? total_info[obj] : "0.0"  )
      .map(obj => typeof obj == 'number' ? obj : parseFloat(obj.replace(/,/g, ''))).reduce(reducer,0.0).toFixed(2)
      
    }
  }

  // let payment_info = bodyData.children.find(x => x.name == 'payment') == undefined ? "O" : bodyData.children.find(x => x.name == 'payment').children.find(x => x.name=="CODE").value
  // let payment_info = typeof bodyData.payment == 'object' ? bodyData.payment[0].CODE : "0"

  let total_value = total_info.TOTALGROSSAMOUNT.toString().replace(/,/g, '')
  let base_value = total_info.TOTALNETAMOUNT.toString().replace(/,/g, '')
  let vat_value = getTaxValues(pms_vat_tax_string)
  let svc_value = getTaxValues(pms_svc_tax_string)
  let state_value = getTaxValues(pms_state_tax_string)
  let final_result

  final_result = {
    "bill_number": bill_number,
    "bill_datetime": moment().format().slice(0, -6),
    "total_value": -total_value,
    "net_value": -base_value,
    "bill_taxes": [{
      "rate": vat_tax_rate,
      "base_value": base_value,
      "value": vat_value,
    }],

    "bill_tax_gst": [{
      "rate": state_tax_rate,
      "base_value": base_value,
      "value": state_value
    }],

    "bill_tax_other": [{
      "tax_name": "Service Charge",
      "rate": svc_tax_rate,
      "base_value": base_value,
      "value": svc_value
    }],
  }

  if (roomNumber != ""){
    final_result.room_number = roomNumber
  }

  let dbMessage = { fiscal_message: message, source: "PMS", system_bill_number: bill_number, void_status: `VOIDED-${rowID}`}
  let newrowID = await insertMessage(dbMessage);
  
  return [newrowID, final_result, bill_number];
  // return final_result
  // return [newRowID,final, bill_number]
}

const makeResponse = (command,message) => {
  let return_code
  let status_code
  let fiscal_folio_number 
  if(message==false){
    return_code = "Message not sent"
    status_code = 'OK'
    fiscal_folio_number = 'null'
  }else{
    return_code = message.hasOwnProperty('errno') ? 'Fiscal IFC Down' : message.returnCodeLink
    status_code = message.hasOwnProperty('errno') ? 'error' : 'SUCCESS'
    fiscal_folio_number = message.hasOwnProperty('errno') ? 'null' : message.FiscalFolioNo
  }
  let final_result = `
  <?xml version="1.0" ?>
  <command>${command}</command>
  <head>
  <message_status>${status_code}</message_status>
  <message>
    ${return_code}
  </message>
  </head>
  <body>
  <fiscal_folio_number>${fiscal_folio_number}</fiscal_folio_number>
  </body>
  `
  return final_result
}

const getCallback = (xmlResponse,jsonResponse,server_params,callback_url,jndiname) => {

  let options = {
    rejectUnauthorized : false,
    headers: { 
      'Content-Type': 'text/xml',
    },
    rejectUnauthorized: false
  }
  let return_code
  let status_code
  let fiscal_folio_number 
  if(jsonResponse==false){
    return_code = "Message not sent"
    status_code = 'SUCCESS'
    fiscal_folio_number = 'null'
  }else{
    return_code = jsonResponse.hasOwnProperty('errno') ? 'Fiscal IFC Down' : jsonResponse.returnCodeLink
    status_code = jsonResponse.hasOwnProperty('errno') ? 'error' : 'SUCCESS'
    fiscal_folio_number = jsonResponse.hasOwnProperty('errno') ? 'null' : jsonResponse.FiscalFolioNo
  }
  // let return_code = jsonResponse.hasOwnProperty('errno') ? 'Fiscal IFC Down' : jsonResponse.returnCode
  // let status_code = jsonResponse.hasOwnProperty('errno') ? 'error' : 'SUCCESS'
  // let fiscal_folio_number = jsonResponse.hasOwnProperty('errno') ? 'null' : jsonResponse.FiscalFolioNo
  let new_params

  new_params = server_params.split(/@/g).map( xy  => {
    if(xy.match('in_mesg_status')){
      return `in_mesg_status=!${status_code}!`
    }else if( xy.match('in_mesg_text') ){
      return `in_mesg_text=!${return_code}!`
    }else if( xy.match('in_fiscal_bill_no') ){
      return `in_fiscal_bill_no=!${fiscal_folio_number}!`
    }
    else{
      return xy
    }
  })

  // console.log(new_params)

  let new_callback_url = jndiname == undefined ? `${callback_url}?${new_params.join("@")}` : `${callback_url}?${new_params.join("@")}&jndiname=${jndiname}`

  needle.post(new_callback_url, "", options, (err, resp) => {
    if (err) {
      console.log(err)
    }else{
      // console.log(resp.body)
    }
  });

}

exports.parseLegacy = parseLegacy