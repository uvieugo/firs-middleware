const {generateFirsMessage} = require('../firs/firs_message');
const {generatePmsResponse} = require('./pms_response');
const {insertMessage, getConfig, getBillNumber, getMessage, updateMessage} = require("../connection/db");
const {postToFirs} = require('../firs/firs_post');
const {voidBill} = require('./pms_void');
const moment = require('moment');
const fs = require('fs');
const parsePmsData = async (ctx) => {
  try {
    let billNo = ctx.DocumentInfo.BillNo
    
    const no_post_response = {
      FiscalFolioNo: billNo,
      FiscalOutputs: {
          Output: [
              {
                  Name: "IGNORE",
                  Value: "not sent"
              }
          ]
      },
      StatusMessages: {
          Messages: [
              {
                  Description: "not sent to tax authority",
                  Type: "Warning"
              }
          ]
      }
    }
  
    if (ctx.hasOwnProperty("FolioInfo")){
      try {
        if (ctx.FolioInfo.TotalInfo.hasOwnProperty("GrossAmount")){
          let amount = ctx.FolioInfo.TotalInfo.GrossAmount
          if (amount == "0" || amount == 0){
            return no_post_response
          }
        }else{
          let amount = ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").map(obj => obj.GrossAmount).filter(obj => obj ).reduce(reducer,0.0)
          if (amount == "0" || amount == 0){
            return no_post_response
          }
        }
        
      } 
      catch (error) {
        // console.log(error)
      }
  
      // check if bill number exists
      let prevMessageId = await getBillNumber(billNo, "PMS")
      let voidProcess
      // if row with bill number exist start and wait for void process
      if (prevMessageId !=null){
        // void previous bill if bill number exists
        voidProcess = await voidBill(prevMessageId)
      }
  
      // Save message from pms to database to get rowId to be used as a sequential bill number
      // let rowID = (await insertMessage(ctx, "", "PMS", bill_number)).join();
      // let dbMessage = { fiscal_message: ctx, source: "PMS", system_bill_number: bill_number}
  
      // let rowID = await insertMessage(dbMessage);
      let [rowID,from_pms_message, bill_number] = await parsePMSMessage(ctx)
  
      // set bill number on from_pms_message to rowId
      from_pms_message.bill_number = rowID
  
      // Generate message to be sent to FIRS Servers
      let firsMessage = await generateFirsMessage(from_pms_message, 'PMS')
      // console.log(firsMessage)
  
      // update databse with generated firs message
      updateMessage(rowID,{message_to_firs:JSON.stringify(firsMessage)})
      // updateFirs(rowID,JSON.stringify(firsMessage))
      // console.log(firsMessage)
  
      // Save message from pms and firs message
      // let rowID = (await insertMessage(ctx, firsMessage, "PMS")).join();
  
      // Send Message to FIRS Servers and collect response
      // let response = await postToFirs(firsMessage).then((response) => {
      let response = postToFirs(firsMessage).then((response) => {
        //Parse response into require PMS format
        return generatePmsResponse(bill_number, response, rowID);
  
      });
    
      let makeTmpResponse = firsMessage.bill
    
      let temp_response = {
        FiscalFolioNo: bill_number,
        FiscalOutputs: {
          Output: [{
            Name: "Security Code",
            Value: makeTmpResponse.security_code
          },{
            Name: "Security Code Link",
            Value: `https://ecitizen.firs.gov.ng/en/security-code-verify/${makeTmpResponse.vat_number}~${makeTmpResponse.bill_number}~${makeTmpResponse.business_place}~${makeTmpResponse.business_device}~${makeTmpResponse.bill_datetime}~${makeTmpResponse.total_value}~${makeTmpResponse.security_code}`
    
          }]
        },
        StatusMessages: {
          Messages: [{
            Description: "Quick response message",
            Type: "Warning"
          }]
        }
      }
  
      return temp_response
  
    }
    else{
      return no_post_response;
    }
    
  } catch (error) {
    console.log(error)
  }

}

async function parsePMSMessage(ctx){
  let {pms_business_device, pms_vat_tax_string, pms_state_tax_string, pms_svc_tax_string, vat_tax_rate, svc_tax_rate, state_tax_rate, mask_guest_info} = await getConfig()
  pms_state_tax_string = pms_state_tax_string.split(",")
  pms_svc_tax_string = pms_svc_tax_string.split(",")
  pms_vat_tax_string = pms_vat_tax_string.split(",")
  let reducer = (a, c) => a + c
  let business_device = pms_business_device
  let bill_number = ctx.DocumentInfo.BillNo
  let bill_datetime = ctx.DocumentInfo.BusinessDateTime

  let payment_type = "0"
  let total_info
  let postingTotal
  let netPostingTotal
  let vatPostingTotal
  let svcPostingTotal
  let statePostingTotal
  let trxCodes = (process.env.FLIP_VAT_CODES + process.env.FLIP_SVC_CODES + process.env.FLIP_STATE_CODES).split(",")
  let vatTrxCodes = process.env.FLIP_VAT_CODES.split(",")
  let svcTrxCodes = process.env.FLIP_SVC_CODES.split(",")
  let stateTrxCodes = process.env.FLIP_STATE_CODES.split(",")
 
  total_info = ctx.FolioInfo.TotalInfo
  if(total_info){
    postingTotal = total_info.hasOwnProperty("GrossAmount") ?  total_info.GrossAmount : 0
    netPostingTotal = total_info.hasOwnProperty("NetAmount") ?  total_info.NetAmount : 0
    vatPostingTotal =  total_info.Taxes.hasOwnProperty("Tax") ? total_info.Taxes.Tax.filter(obj => pms_vat_tax_string.includes(obj.Name)).map(obj => obj.Value).reduce(reducer,0.0) : 0
    svcPostingTotal =  total_info.Taxes.hasOwnProperty("Tax") ? total_info.Taxes.Tax.filter(obj => pms_svc_tax_string.includes(obj.Name)).map(obj => obj.Value).reduce(reducer,0.0) : 0
    statePostingTotal = total_info.Taxes.hasOwnProperty("Tax") ? total_info.Taxes.Tax.filter(obj => pms_state_tax_string.includes(obj.Name)).map(obj => obj.Value).reduce(reducer,0.0) : 0
  }else{
    postingTotal = ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").map(obj => obj.GrossAmount).filter(obj => obj ).reduce(reducer,0.0)
    netPostingTotal = ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => !trxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
    vatPostingTotal =  ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => vatTrxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
    svcPostingTotal =  ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => svcTrxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
    statePostingTotal = ctx.FolioInfo.Postings.filter(obj => obj.TrxType== "C").filter(obj => stateTrxCodes.includes(obj.TrxCode)).map(obj => obj.NetAmount).filter(obj => obj ).reduce(reducer,0.0)
  }

  

  function setBillDateTime(){
    return (process.env.NODE_ENV != 'development' ? moment().format().slice(0, -6) : bill_datetime)
  }

  let from_pms_message = {
    "bill_number": bill_number,
    "fiscal_folio_id": bill_number,
    "bill_datetime": setBillDateTime(),
    "payment_type": payment_type,
    "total_value": parseFloat(postingTotal).toFixed(2),
    "tax_free": "0",
    "base_value": parseFloat(netPostingTotal).toFixed(2),
    "business_device": business_device,
    "bill_taxes": [{
      "rate": vat_tax_rate,
      "base_value": parseFloat(netPostingTotal).toFixed(2),
      "value": (vatPostingTotal > 0 ? parseFloat(vatPostingTotal).toFixed(2) : "0.00")
    }],

    "bill_tax_gst": [{
      "rate": state_tax_rate,
      "base_value": parseFloat(netPostingTotal).toFixed(2),
      "value": parseFloat(statePostingTotal).toFixed(2)
    }],

    "bill_tax_other": [{
      "tax_name": "Service Charge",
      "rate": svc_tax_rate,
      "base_value": parseFloat(netPostingTotal).toFixed(2),
      "value": parseFloat(svcPostingTotal).toFixed(2)
    }]
  }

  if (mask_guest_info){
    if (ctx.hasOwnProperty("FolioInfo")){
      let payee_info = ctx.FolioInfo.PayeeInfo
      payee_info.FirstName = ""
      payee_info.LastName = ""
      payee_info.Email = ""
      payee_info.Address = ""
      payee_info.Phone = ""
      payee_info.Salutation = ""

    }
    if(ctx.hasOwnProperty("ReservationInfo")){
      // console.log("i a, here")
      let guest_info = ctx.ReservationInfo.GuestInfo
      guest_info.FirstName = ""
      guest_info.LastName = ""
      guest_info.Email = ""
      guest_info.Address = ""
      guest_info.Phone = ""
    }
  }

  let dbMessage = { fiscal_message: ctx, source: "PMS", system_bill_number: bill_number}
  let rowID = await insertMessage(dbMessage);
  
  return [rowID, from_pms_message, bill_number];

}


exports.parsePmsData = parsePmsData