const { updateMessage } = require("../connection/db");

const generatePmsResponse = (folioNumber, response, rowID) => {
  // console.table(response)
  let pmsResponse
  if(response.hasOwnProperty('error')){
    pmsResponse = {
      FiscalFolioNo: folioNumber,
      FiscalOutputs: {
        Output: [{
          Name: "Security Code",
          Value: response.security_code
        },{
          Name: "Security Code Link",
          Value: `https://ecitizen.firs.gov.ng/en/security-code-verify/${response.vat_id}~${response.bill_number}~${response.office_id}~${response.device_number}~${response.date_time}~${response.total}~${response.security_code}`

        }]
      },
      StatusMessages: {
        Messages: [{
          Description: response.error.code,
          Type: "Warning"
        }]
      }
    } 
    updateMessage(rowID,{response_code: response.error.code, success: false, error_response: JSON.stringify(response.error_message)}); 
  }else{
    pmsResponse = {
      FiscalFolioNo: folioNumber,
      FiscalOutputs: {
        Output: [{
          Name: "Payment Code",
          Value: response.payment_code
        },{
          Name: "Payment Code Link",
          Value: `https://ecitizen.firs.gov.ng/en/payment-code-verify?code=${response.payment_code}`
        },
        {
          Name: "Security Code",
          Value: response.security_code
        }]
      },
      StatusMessages: {
        Messages: [{
          Description: "Submission Successful.",
          Type: "Warning"
        }]
      }
    }
    updateMessage(rowID,{firs_response: response.payment_code, success: true, response_code: response.status_code})
  }
  return pmsResponse;
}

exports.generatePmsResponse = generatePmsResponse;