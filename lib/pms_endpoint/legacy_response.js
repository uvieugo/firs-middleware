const { updateMessage } = require("../connection/db");

const generateLegacyResponse = (folioNumber, response, rowID) => {
  // console.table(response)
  let pmsResponse
  if(response.hasOwnProperty('error')){
    pmsResponse = {
      FiscalFolioNo: folioNumber,
      returnCode: response.security_code,
      returnCodeLink: `https://ecitizen.firs.gov.ng/en/security-code-verify/${response.vat_id}~${response.bill_number}~${response.office_id}~${response.device_number}~${response.date_time}~${response.total}~${response.security_code}`
    }
    updateMessage(rowID,{response_code: response.error.code, success: false});
    // dbUpdate(rowID,"",response.error.code,"error");
  }else{
    pmsResponse = {
      FiscalFolioNo: folioNumber,
      returnCode: response.payment_code,
      returnCodeLink: `https://ecitizen.firs.gov.ng/en/payment-code-verify?code=${response.payment_code}`,
    }
    updateMessage(rowID,{firs_response: response.payment_code, response_code: response.status_code, success: true})
    // dbUpdate(rowID,response.payment_code,response.status_code,"success"); 
  }
  return pmsResponse;
}

exports.generateLegacyResponse = generateLegacyResponse;