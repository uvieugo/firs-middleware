const fs = require('fs');
const net = require('net');
const logger = require('./logger')
const {parsePosMessage} = require("./pos_endpoint/parse_pos_message");
const { pos_port } = require('./config/config');

class tcpServer {

  constructor(portNumber){
    this.portNumber = portNumber
    this.server = net.createServer((socket) => {
      socket.on('connect', () => {
        console.log('Client Connected')
      })
  
      socket.on('end', () => {
        console.log('client disconnected');
      });
  
      socket.on('data', async (data) => {
        // console.log(data)
        // logger.log({level: 'info', 'timeStamp': new Date, 'origin': 'POS', message: data })
        let pos_message = await parsePosMessage(data)
        // logger.info(pos_message)
        socket.write(pos_message);
      });
  
      socket.on('error', (err) => {
        console.log(err)
      })
  
      socket.on('close', () => {
        console.log('socket closed')
      })
    });
    this.server.on('error', (err) => {
      console.log(err)
      logger.error(err)
    })
    this.running = false
  }

  startServer = async () => {
    this.server.listen(this.portNumber, (err) => {
      if(err){
        console.log(err)
        logger.error({label: "Startup Error", message: "error"})
      }else{
        logger.info({label: 'startup', message: `Starting TCP Endpoint on ${this.portNumber}`})
        console.log(`Starting TCP Endpoint on ${this.portNumber}`);
      }
    });
  }

  stopServer = async () => {
    this.server.close(() => {
      console.log('Server is stopped')
    })
  }

  serverStatus = () => {
    this.running = this.server.listening
    return this.running
  }

}

class Singleton {

  constructor() {
      if (!Singleton.instance) {
          Singleton.instance = new tcpServer(pos_port)
      }
  }

  getInstance() {
      return Singleton.instance;
  }

}

// module.exports = new tcpServer(pos_port)
module.exports = new Singleton().getInstance()