const express = require('express');
const helmet = require("helmet");
const createError = require('http-errors');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const debug = require('debug')('myapp:server');
const http = require('http')
const rfs = require('rotating-file-stream')
const {parsePmsData} = require('./pms_endpoint/parse_pms_data')
// const {parseSIMHFIData} = require('./pos_endpoint/hfi/hfi_sim_parse')
// const {parseRESHFIData} = require('./pos_endpoint//hfi/hfi_res_parse')
const {parseLegacy} = require('./pms_endpoint/legacy')
const {pms_port} = require('./config/config');
const altLogger = require('./logger')

const app = express();
app.use(helmet());
const accessLogStream = rfs.createStream('access.log', {
  interval: '1d', // rotate daily
  path: path.join(__dirname, 'log')
})
app.use(logger('combined', { stream: accessLogStream }));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
app.use(express.text())
app.use(cookieParser());

app.get('/service', (req, res) => {
  res.send('Http Service Running')
})

app.post('/PMS_FLIP', async (req,res) => {
  altLogger.log({level: 'info', 'label': 'FROMPMS', message: JSON.stringify(req.body) })
  let response = await parsePmsData(req.body)
  altLogger.log({level: 'info', 'label': 'FROMFIRSTOPMS', message: JSON.stringify(response) })
  res.json(response)
})

app.post('/PMS_LEGACY', async (req,res) => {
  altLogger.log({level: 'info', 'label': 'FROMPMS', message: JSON.stringify(req.body) })
  let response = parseLegacy(req.query)
  altLogger.log({level: 'info', 'label': 'FROMFIRSTOPMS', message: JSON.stringify(response) })
  res.sendStatus(200)
})

// app.post('/HFI_SIM', async (req,res) => {
//   altLogger.log({level: 'info', 'label': 'PMS', message: req.body })
//   let response = await parseSIMHFIData(req.body)
//   altLogger.info(response)
//   res.send(response)
// })

// app.post('/HFI_RES', async (req,res) => {
//   altLogger.log({level: 'info', 'label': 'PMS', message: req.body })
//   let response = await parseRESHFIData(req.body)
//   altLogger.info(response)
//   res.send(response)
// })

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send('error');
});

// module.exports = app;

class httpServer{
  constructor(app,port){
    this.server = http.createServer(app);
    this.port = port;
    this.running = false
  }

  startServer = async () => {
    this.server.listen(this.port);
    this.server.on('error', this.onError);
    this.server.on('listening', this.onListening);
  }

  stopServer = async () => {
    this.server.close()
  }

  serverStatus = () => {
    this.running = this.server.listening
    return this.running;
  } 
  
  onError = (error) => {
    if (error.syscall !== 'listen') {
      throw error;
    }
  
    var bind = typeof this.port === 'string'
      ? 'Pipe ' + this.port
      : 'Port ' + this.port;
  
    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        altLogger.error(bind + ' requires elevated privileges')
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        altLogger.error(bind + ' is already in use')
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }
  
  onListening = () => {
    var addr = this.server.address();
    var bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port;
    // debug('Listening on ' + bind);
    altLogger.info({label: 'Startup', message: `Http Endpoint Listening on ${bind}`})
    console.log('Http Endpoint Listening on ', bind)
  }

}

module.exports = new httpServer(app,pms_port)