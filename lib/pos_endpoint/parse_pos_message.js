const {parseRes} = require('./res/parse_res')
const {parseSimphony} = require('./simphony/parse_simphony')
const {parseM9700} = require('./m9700/parse_m9700')
const logger = require('../logger')

const parsePosMessage = async (data) => {

  let bufferData = Buffer.from(data).toString('ascii');
  console.log(bufferData)
  
  let posId = bufferData.slice(bufferData.indexOf('\u0001') + 1, bufferData.indexOf('\u0002'));
  let startPoint = bufferData.indexOf('\u0002') + 1;
  let endPoint = bufferData.indexOf('\u0003');
  let messageData = bufferData.slice(startPoint, endPoint);
  let messageArray = messageData.split('\u001c');
  let posType = messageArray[2];
  
  if (posId.trim() == "0") {
    return(data)
  }else{
    if (posType== 'RES'){
      // return await parseRes(bufferData)
      logger.log({level: 'info', 'label': 'POS-RES', message: bufferData })
      return await parseRes(posId,messageArray)
    }
    if (posType== 'M9700'){
      // return await parseM9700(bufferData)
      logger.log({level: 'info', 'label': 'POS-M9700', message: bufferData })
      return await parseM9700(posId,messageArray)
    }
    if (posType== 'SIMPHONY'){
      // console.log(messageArray)
      // return await parseSimphony(bufferData)
      logger.log({level: 'info', 'label': 'POS-SIMPHONY', message: bufferData })
      return await parseSimphony(posId,messageArray)

    }
  }

  
}

exports.parsePosMessage = parsePosMessage