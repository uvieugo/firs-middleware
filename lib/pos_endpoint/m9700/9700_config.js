const {getConfig} = require('../../connection/db');

const m9700Config = async (resultArr) => {
  let { vat_tax_rate,svc_tax_rate,state_tax_rate,pos_config_type } = await getConfig()
  let vat_tax_value
  let svc_tax_value
  let state_tax_value
  let vatTaxRate = parseFloat(vat_tax_rate)
  let svcTaxRate = parseFloat(svc_tax_rate)
  let stateTaxRate = parseFloat(state_tax_rate)
  let total_taxes = vatTaxRate + stateTaxRate + svcTaxRate
  let reducer = (a, c) => a + c
  
  let tenderString = resultArr[5] 
  let [checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal] = await parseTenders(tenderString)
  // console.log(taxableTenders)

  let checkDetailString = resultArr[9]
  let checkData = await parseCheckItems(checkDetailString)
  // console.log(checkData)

  let payment_type = taxableTenders.length == 0 ? "0" :  taxableTenders[0][1]
  
  // 1. Inclusive, Exclusive (Vat,SVC,LCT) SVC as TAX
  if(pos_config_type==1){
      
    total_value = checkData.map(e => e[0] ).reduce(reducer,0.0)
    net_value = checkData.map(e => e[1] ).reduce(reducer,0.0)
    vat_tax_value = checkData.map(e => e[2] ).reduce(reducer,0.0)
    state_tax_value = checkData.map(e => e[3] ).reduce(reducer,0.0)
    svc_tax_value = checkData.map(e => e[4] ).reduce(reducer,0.0)

    if (noTaxTenders.length !== 0){
      let mypercentage = (taxableTenderTotal/checkTendersTotal) * 100
      vat_tax_value =  (mypercentage/100) * vat_tax_value
      state_tax_value = (mypercentage/100) * state_tax_value
      svc_tax_value = (mypercentage/100) * svc_tax_value
      net_value = (mypercentage/100) * net_value
    }
    // console.log(vat_tax_value, state_tax_value, svc_tax_value, net_value, checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal, payment_type)
    return [ vat_tax_value, state_tax_value, svc_tax_value, net_value, checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal, payment_type]
  }

  //2. Exclusive(Vat,SVC,LCT) SVC as Auto Service Charge
  if(pos_config_type==2){
    vat_tax_value = parseFloat(resultArr[6])
    state_tax_value = parseFloat(resultArr[7])
    svc_tax_value = parseFloat(resultArr[12]) + parseFloat(resultArr[13])
    total_value = checkTendersTotal
    net_value = checkTendersTotal - (vat_tax_value + svc_tax_value + state_tax_value)

    if (noTaxTenders.length !== 0){
      let mypercentage = (taxableTenderTotal/checkTendersTotal) * 100
      vat_tax_value =  (mypercentage/100) * vat_tax_value
      state_tax_value = (mypercentage/100) * state_tax_value
      svc_tax_value = (mypercentage/100) * svc_tax_value
      net_value = (mypercentage/100) * net_value
    }

    return [ vat_tax_value, state_tax_value, svc_tax_value, net_value, checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal, payment_type]
  
  }

}

async function parseTenders(tenderString){
  let { pos_not_taxable} = await getConfig()
  pos_not_taxable = pos_not_taxable.split(",")
  let reducer = (a, c) => a + c
  let checkTenders = tenderString
    .split("||") //Split tender string by || character
    .filter(e => e) // remove blanks
    .map(obj => obj.split("|-") ) // map through and split again by |-
    //.filter(obj => !pos_not_taxable.includes(obj[1])) // filter only taxable tenders
     
  let checkTendersTotal = checkTenders.map(obj => {
    let priceField = obj[3]
    let result = priceField.endsWith("-") ? parseFloat(-(priceField.slice(0,-1))) : parseFloat(priceField)
    return result
  } ).reduce(reducer,0.0)

  let noTaxTenders = checkTenders.filter(obj => pos_not_taxable.includes(obj[1]))
  let taxableTenders = checkTenders.filter(obj => !pos_not_taxable.includes(obj[1]))
  
  let taxableTenderTotal = taxableTenders.map(obj => {
    let priceField = obj[3]
    let result = priceField.endsWith("-") ? parseFloat(-(priceField.slice(0,-1))) : parseFloat(priceField)
    return result
  } ).reduce(reducer,0.0)


  return [checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal]
}

function taxedApplied(taxTypeString){
  let newTaxTypeString = taxTypeString.slice(0,2)
  let taxTypes = (parseInt(newTaxTypeString, 16).toString(2)).padStart(8, '0').split("")
  let result = taxTypes.map( (element,index) => {
    if(element == "1"){
      return (index+1).toString()
    }
  }).filter(e => e);
  // console.log(result)
  return result
}

async function parseCheckItems(checkDetailString){
  let { pos_tax_combined,pos_state_tax_string,pos_vat_tax_string,pos_svc_tax_string,vat_tax_rate,svc_tax_rate,state_tax_rate} = await getConfig()
  let vat_tax_value
  let svc_tax_value
  let state_tax_value
  let vatTaxRate = parseFloat(vat_tax_rate)
  let svcTaxRate = parseFloat(svc_tax_rate)
  let stateTaxRate = parseFloat(state_tax_rate)
  let total_taxes = vatTaxRate + stateTaxRate + svcTaxRate

  let checkDetails = checkDetailString
    .split("||")
    .filter(e => e)
    .map(obj => obj.split("|-"))
    .filter(obj => obj[5] == '0')
    
    // .filter( obj => obj.split("|-")[5] == "0" )
        
  let checkItems = checkDetails.map((item) => {
    
    // Split each detail item to get its total value and applied tax string
    // eg Total Value = 7500.00, applied tax string = E000000000000000
    let itemDetail = item
    
    // Get the current detail itemType, values are "M", "D", "S"
    let itemType = itemDetail[0]
    
    // Find the taxes applied on the current check detail item
    // taxstring is stored in itemDetail[4]
    let taxType = taxedApplied(itemDetail[4])
    
    // Current currentDetail total value
    let total_value = parseFloat(itemDetail[3])

    let isVatable = taxType.includes(pos_vat_tax_string)

    // console.log(isVatable)

    // calculate the taxes applied.
    if (pos_tax_combined){
      vat_tax_value = parseFloat( ((vatTaxRate/(100 + total_taxes)) * (total_value)).toFixed(2) )
      state_tax_value = state_tax_active ? parseFloat( ((stateTaxRate/(100 + total_taxes)) * (total_value)).toFixed(2)    ) : 0.0
      svc_tax_value = service_charge_active ? parseFloat( ((svcTaxRate/(100 + total_taxes)) * (total_value)).toFixed(2)  )  : 0.0
    }
    else{

      vat_tax_value = taxType.includes(pos_vat_tax_string) ? parseFloat( ((vatTaxRate/(100 + total_taxes)) * (total_value)).toFixed(2)   ) : 0.0
      state_tax_value = taxType.includes(pos_state_tax_string) ? parseFloat(  ((stateTaxRate/(100 + total_taxes)) * (total_value)).toFixed(2)    ) : 0.0
      svc_tax_value = taxType.includes(pos_svc_tax_string) ? parseFloat( ((svcTaxRate/(100 + total_taxes)) * (total_value)).toFixed(2)  )  : 0.0
    }

    // net value of the current checkDetail Item.
    let net_value = total_value - vat_tax_value - state_tax_value - svc_tax_value

    // return array containing [total_value, net_value, vat_tax_value, state_tax_value, svc_tax_value]
    // for itemType = "D" (Discount), return negative values
    if (itemType == 'D') {
      return [-total_value, -net_value, -vat_tax_value, -state_tax_value, -svc_tax_value, isVatable]
    }else{
      return [total_value, net_value, vat_tax_value, state_tax_value, svc_tax_value, isVatable]
    }
  })

  return checkItems
}

exports.m9700Config = m9700Config