const create_res_message = (posType, posId, response, rowID, messageId, command, inAppSeq) => {

  let seqNum = inAppSeq
  let pos_message;
  const appSeq = (seqNum > 9) ? `${seqNum} ` : `0${seqNum} `;
  const SOH = String.fromCharCode(01);
  const STX = String.fromCharCode(02);
  const ETX = String.fromCharCode(03);
  const EOT = String.fromCharCode(04);
  const FS = String.fromCharCode(28);
  const messId = messageId
  let split_message
  let message
  let message_count
  let payment_code
  let security_code

  if (response.hasOwnProperty('payment_code')){
    security_code = response.security_code
    payment_code = response.payment_code
      split_message= splitString(`https://ecitizen.firs.gov.ng/en/payment-code-verify?code=${response.payment_code}`)
      message = split_message.join(FS)
      message_count = split_message.length
      pos_message = SOH + posId + STX + FS + appSeq + messId + FS + "Y" + FS + payment_code + FS + security_code + FS + message_count + FS + message  + ETX + EOT;
  }else if (response.hasOwnProperty('security_code')){
    split_message = splitString(`https://ecitizen.firs.gov.ng/en/security-code-verify/${response.vat_id}~${response.bill_number}~${response.office_id}~${response.device_number}~${response.date_time}~${response.total}~${response.security_code}`)
    message = split_message.join(FS)
    message_count = split_message.length
    pos_message = SOH + posId + STX + FS + appSeq + messId + FS +  "Y" + FS + message_count + FS + message + ETX + EOT;
  }else if(response=="NOPRINT"){
    message = "N"
    pos_message = SOH + posId + STX + FS + appSeq + messId + FS + message + ETX + EOT;
  }
  else{
    message = "Looking Good!!!"
    pos_message = SOH + posId + STX + FS + appSeq + messId + FS + message + ETX + EOT;
  }
  console.log(pos_message)
  return pos_message;
  }

const splitString = (input) => {
  let result = []
  const splitValue = 32

  for (let index = 0; index < input.length; index=index+splitValue) {
    result.push(input.slice(index, index+splitValue))
    
  }
  return result
}

exports.create_res_message = create_res_message