const { getConfig } = require('../../connection/db');

const simConfig = async (resultArr) => {
  let {pos_config_type} = await getConfig()
  let vat_tax_value
  let state_tax_value
  let svc_tax_value
  let net_value
  let total_value
  let payment_type = resultArr[9]  

  // 1. Inclusive, Exclusive (Vat,SVC,LCT) SVC as TAX
  if(pos_config_type==1){
    vat_tax_value = parseFloat(resultArr[6])
    state_tax_value = parseFloat(resultArr[7])
    svc_tax_value = parseFloat(resultArr[8]) + parseFloat(resultArr[15]) + parseFloat(resultArr[14])
    total_value = parseFloat(resultArr[4])
    net_value = total_value - (vat_tax_value + svc_tax_value + state_tax_value)

    return [ vat_tax_value, state_tax_value, svc_tax_value, net_value, total_value, payment_type]

  }

}

async function parseTenders(tenderString){
  let {pos_not_taxable} = await getConfig()
  let reducer = (a, c) => a + c
  let checkTenders = tenderString
    .split("||") //Split tender string by || character
    .filter(e => e) // remove blanks
    .map(obj => obj.split("|-") ) // map through and split again by |-
    //.filter(obj => !pos_not_taxable.includes(obj[1])) // filter only taxable tenders
     
  let checkTendersTotal = checkTenders.map(obj => {
    let priceField = obj[3]
    let result = priceField.endsWith("-") ? parseFloat(-(priceField.slice(0,-1))) : parseFloat(priceField)
    return result
  } ).reduce(reducer,0.0)

  let noTaxTenders = checkTenders.filter(obj => pos_not_taxable.includes(obj[1]))
  let taxableTenders = checkTenders.filter(obj => !pos_not_taxable.includes(obj[1]))
  
  let taxableTenderTotal = taxableTenders.map(obj => {
    let priceField = obj[3]
    let result = priceField.endsWith("-") ? parseFloat(-(priceField.slice(0,-1))) : parseFloat(priceField)
    return result
  } ).reduce(reducer,0.0)


  return [checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal]
}

exports.simConfig = simConfig