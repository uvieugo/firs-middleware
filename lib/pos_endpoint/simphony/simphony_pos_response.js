const create_sim_message = (posType, posId, response, rowID, messageId, command) => {
  let pos_message;
  const SOH = String.fromCharCode(01);
  const STX = String.fromCharCode(02);
  const ETX = String.fromCharCode(03);
  const EOT = String.fromCharCode(04);
  const FS = String.fromCharCode(28);
  const appSeq = "01 ";
  const messId = messageId
  let split_message
  let message
  let message_count
  let message2
  let payment_code
  let security_code

  if (response.hasOwnProperty('payment_code')){
    security_code = response.security_code
    payment_code = response.payment_code
      split_message= splitString(`https://ecitizen.firs.gov.ng/en/payment-code-verify?code=${response.payment_code}`)
      message = split_message.join(FS)
      message_count = split_message.length
      message2 = `https://ecitizen.firs.gov.ng/en/payment-code-verify?code=${response.payment_code}`
      pos_message = SOH + posId + STX + FS + appSeq + messId + FS + "Y" + FS + payment_code + FS + security_code + FS + message_count + FS + message + FS + message2 + ETX + EOT;
  }else if (response.hasOwnProperty('security_code')){
    security_code = response.security_code
    split_message = splitString(`https://ecitizen.firs.gov.ng/en/security-code-verify/${response.vat_id}~${response.bill_number}~${response.office_id}~${response.device_number}~${response.date_time}~${response.total}~${response.security_code}`)
    message = split_message.join(FS)
    message_count = split_message.length
    message2 = `https://ecitizen.firs.gov.ng/en/security-code-verify/${response.vat_id}~${response.bill_number}~${response.office_id}~${response.device_number}~${response.date_time}~${response.total}~${response.security_code}`
    pos_message = SOH + posId + STX + FS + appSeq + messId + FS +  "Y"  + FS + "" + FS + security_code + FS + message_count + FS + message + FS + message2 + ETX + EOT;
  }else if(response=="NOPRINT"){
    message = "N"
    pos_message = SOH + posId + STX + FS + appSeq + messId + FS + message + ETX + EOT;
  }
  else{
    if(command=="VOID"){
      message = response
    }else{  
      message = "Looking Good!!!"
    }
    pos_message = SOH + posId + STX + FS + appSeq + messId + FS + message + ETX + EOT;
  }

  // if(messageId=='FinalTender'){
  //   if (response.hasOwnProperty('error')){
  //     // dbUpdate(rowID,"",response.error.code,"error");
  //     dbUpdate(rowID,"",response.error.code,"error",JSON.stringify(response.error_message)); 
  //   }else{
  //     dbUpdate(rowID,response.payment_code,response.status_code,"success"); 
  //   }
  // }
  // pos_message = SOH + posId + STX + FS + appSeq + messId + FS + message_count + FS + message + ETX + EOT;
  console.log(pos_message)
  return pos_message;
  }


const splitString = (input) => {
  let result = []
  const splitValue = 30

  for (let index = 0; index < input.length; index=index+splitValue) {
    result.push(input.slice(index, index+splitValue))
    
  }
  return result
}

exports.create_sim_message = create_sim_message