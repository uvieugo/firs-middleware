const moment = require('moment');
const {generateFirsMessage} = require('../../firs/firs_message');
const {insertMessage, getConfig, updateMessage, getBillNumber, getMessage} = require("../../connection/db");
const {postToFirs} = require('../../firs/firs_post');
// const { pos_not_taxable,pos_taxes_combined,pos_vat_tax_rate,pos_svc_tax_rate,pos_state_tax_rate} = require('../../config/config');
const {create_sim_message} = require('./simphony_pos_response');
const logger = require("../../logger");
const {simConfig} = require('./sim_config')

const parseSimphony = async (posId,messageArray) => {
  console.log(messageArray)
  let {pos_not_taxable, pos_tax_combined, vat_tax_rate, svc_tax_rate, state_tax_rate} = await getConfig()
  return new Promise(async (resolve) => {

    let messageId = messageArray[1].split(" ")[1]
    let posType = messageArray[2];
    let bill_number = messageArray[3]
    let business_device = messageArray[10]

    // let total_value
    let total_taxes
    let tax_free = 0.00
    let message_to_pos
    let reducer = (a, c) => a + c
    let combined_tax_rate
    let vatTaxRate = parseFloat(vat_tax_rate)
    let svcTaxRate = parseFloat(svc_tax_rate)
    let stateTaxRate = parseFloat(state_tax_rate)

    if (pos_tax_combined){
      combined_tax_rate = parseFloat(messageArray[6])
      total_taxes = combined_tax_rate
    }else{
      total_taxes = vatTaxRate + stateTaxRate + svcTaxRate
    }

    switch (messageId) {
      case 'FinalTender':
        let [ vat_tax_value, state_tax_value, svc_tax_value, net_value, total_value, payment_type ] = await simConfig(messageArray)

        // if (taxableTenders.length == 0){
        if (pos_not_taxable.split(",").includes(payment_type)){
          resolve(create_sim_message(posType, posId, "NOPRINT", "", messageId, ""))
        }
        else{

          // total_value = taxableTenderTotal
          // console.log(net_value)
          let from_pos_message = {
            "bill_number": bill_number,
            "bill_datetime": moment().format().slice(0, -6),
            "business_device": business_device,
            "payment_type": payment_type,
            "total_value": total_value,
            "tax_free": tax_free,
            "base_value": net_value,

            "bill_taxes": [{
              "rate": vatTaxRate.toFixed(2).toString(),
              "base_value": net_value.toFixed(2).toString(),
              "value": vat_tax_value.toFixed(2).toString()
            }],
    
            "bill_tax_gst": [{
              "rate": stateTaxRate.toFixed(2).toString(),
              "base_value": net_value.toFixed(2).toString(),
              "value": state_tax_value.toFixed(2).toString()
            }],
    
            "bill_tax_other": [{
              "tax_name": "Service Charge",
              "rate": svcTaxRate.toFixed(2).toString(),
              "base_value": net_value.toFixed(2).toString(),
              "value": svc_tax_value.toFixed(2).toString()
            }]
          }
        
          //  // Save message from pos and firs message
          let rowID = await insertMessage({fiscal_message: from_pos_message, source:"POS", system_bill_number: bill_number})

          // set bill number to row id for sequential bill numbers
          from_pos_message.bill_number = rowID

          let firsMessage = await generateFirsMessage(from_pos_message, 'POS')

          updateMessage(rowID,{message_to_firs:JSON.stringify(firsMessage)})

          console.log(JSON.stringify(firsMessage))
          
          let response = await postToFirs(firsMessage).then(result => result );

          if (response.hasOwnProperty('error')){
            updateMessage(rowID,{response_code: response.error.code, success: false, error_response: JSON.stringify(response.error_message) })
            // dbUpdate(rowID,"",response.error.code,"error",JSON.stringify(response.error_message)); 
          }else{
            updateMessage(rowID,{firs_response: response.payment_code, success: true, response_code: response.status_code})
            // dbUpdate(rowID,response.payment_code,response.status_code,"success"); 
          }
  
          message_to_pos =  create_sim_message(posType, posId, response, rowID, messageId, "")
          logger.info({label: "FIRSTOPOS", message: message_to_pos})
          
        }

        break;
      case 'TestMessage':
        message_to_pos = create_sim_message(posType, posId, "", "", messageId, "");
        break;
      case 'VOIDCHECK':
        // get the bill number
        let voidStatus = await voidCheck(messageArray[3])
        message_to_pos = create_sim_message(posType, posId, voidStatus, "", messageId, "VOID");
        break
      default:
      message_to_pos = create_sim_message(posType, posId, "", "", messageId, "");
      break;
    }

    resolve(message_to_pos)

  })

}

// function taxedApplied(taxTypeString){
//   let newTaxTypeString = taxTypeString.slice(0,2)
//   let taxTypes = (parseInt(newTaxTypeString, 16).toString(2)).padStart(8, '0').split("")
//   let result = taxTypes.map( (element,index) => {
//     if(element == "1"){
//       return (index+1).toString()
//     }
//   }).filter(e => e);
//   return result
// }

async function voidCheck(billNo){

  let rowID = await getBillNumber(billNo)
  console.log(rowID)
  if(rowID==null){
      return "Invalid Check Number"
  }else{
    let rowData = await getMessage(rowID)
    await updateMessage(rowID,{void_status: "VOIDED"})
    let prevMessage = rowData.fiscal_message
    let newMessage = JSON.parse(prevMessage)

    newMessage.total_value = -newMessage.total_value
    newMessage.base_value = -newMessage.base_value

    let newRowID = await insertMessage({fiscal_message: newMessage, source:"POS", system_bill_number: billNo})
    
    // // set bill number to row id for sequential bill numbers
    newMessage.bill_number = newRowID
    let firsMessage = await generateFirsMessage(newMessage, 'POS')
    console.log(firsMessage)    
    
    updateMessage(newRowID,{message_to_firs:JSON.stringify(firsMessage)})
           
    let response = await postToFirs(firsMessage).then(result => result );
  
    if (response.hasOwnProperty('error')){
      updateMessage(newRowID,{response_code: response.error.code, success: false, error_response: JSON.stringify(response.error_message) })
      // dbUpdate(rowID,"",response.error.code,"error",JSON.stringify(response.error_message)); 
      return "An error has occured"
    }else{
      updateMessage(newRowID,{firs_response: response.payment_code, success: true, response_code: response.status_code, void_status: `VOIDED-${rowID}`})
      // dbUpdate(rowID,response.payment_code,response.status_code,"success"); 
      return "Check Void Successful"
    }

  }
}

exports.parseSimphony = parseSimphony