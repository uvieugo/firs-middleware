const moment = require('moment');
const {generateFirsMessage} = require('../../firs/firs_message');
const {insertMessage, updateMessage, getConfig} = require("../../connection/db");
const {postToFirs} = require('../../firs/firs_post');
const {create_res_message} = require('./res_pos_response');
const logger = require("../../logger");
const {resConfig} = require('./res_config');

const parseRes = async (posId,resultArr) => {
  console.log(resultArr)
  let {pos_tax_combined, vat_tax_rate, svc_tax_rate, state_tax_rate} = await getConfig()
  let combined_tax_rate
  let vatTaxRate = parseFloat(vat_tax_rate)
  let svcTaxRate = parseFloat(svc_tax_rate)
  let stateTaxRate = parseFloat(state_tax_rate)
  let total_value
  let total_taxes
  let message_to_pos
  return new Promise(async (resolve) => {

    let messageId = resultArr[1].split(" ")[1]
    let posType = resultArr[2];
    let bill_number = resultArr[3]
    let business_device = resultArr[11]
    let tax_free = 0.0

    if (pos_tax_combined){
      combined_tax_rate = parseFloat(resultArr[5])
      total_taxes = combined_tax_rate
    }else{
      total_taxes = vatTaxRate + stateTaxRate + svcTaxRate
    }
   
    switch (messageId) {
      case 'BeginCheck':
        message_to_pos = create_res_message(posType, posId, "", "", messageId, "");
        break;
      case 'FinalTender': 
        let [ vat_tax_value, state_tax_value, svc_tax_value, net_value, checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal, payment_type ] = await resConfig(resultArr)

        if (taxableTenders.length == 0){
          resolve(create_res_message(posType, posId, "NOPRINT", "", messageId, ""))
        }
        else{

          total_value = taxableTenderTotal

          let from_pos_message = {
            "bill_number": bill_number,
            "bill_datetime": moment().format().slice(0, -6),
            "business_device": business_device,
            "payment_type": payment_type,
            "total_value": total_value,
            "tax_free": tax_free,
            "base_value": net_value,

            "bill_taxes": [{
              "rate": vatTaxRate.toFixed(2).toString(),
              "base_value": net_value.toFixed(2).toString(),
              "value": vat_tax_value.toFixed(2).toString()
            }],
    
            "bill_tax_gst": [{
              "rate": stateTaxRate.toFixed(2).toString(),
              "base_value": net_value.toFixed(2).toString(),
              "value": state_tax_value.toFixed(2).toString()
            }],
    
            "bill_tax_other": [{
              "tax_name": "Service Charge",
              "rate": svcTaxRate.toFixed(2).toString(),
              "base_value": net_value.toFixed(2).toString(),
              "value": svc_tax_value.toFixed(2).toString()
            }]
          }
        
          //  // Save message from pos and firs message
          // let rowID = (await insertMessage(from_pos_message,"","POS")).join()
          let rowID = await insertMessage({fiscal_message: from_pos_message, source:"POS", system_bill_number: bill_number})

          // set bill number to row id for sequential bill numbers
          from_pos_message.bill_number = rowID

          let firsMessage = await generateFirsMessage(from_pos_message, 'POS')

          updateMessage(rowID,{message_to_firs:JSON.stringify(firsMessage)})

          console.log(JSON.stringify(firsMessage))
          
          let response = await postToFirs(firsMessage).then(result => result );

          if (response.hasOwnProperty('error')){
            updateMessage(rowID,{response_code: response.error.code, success: false, error_response: JSON.stringify(response.error_message) })
          }else{
            updateMessage(rowID,{firs_response: response.payment_code, success: true, response_code: response.status_code})
          }
  
          message_to_pos =  create_res_message(posType, posId, response, rowID, messageId, "")
          logger.info({label: "FIRSTOPOS", message: message_to_pos})
          
        }
        break;
      case 'TestMessage':
        message_to_pos = create_res_message(posType, posId, "", "", messageId, "");
        break;
      default:
      message_to_pos = create_res_message(posType, posId, "", "", messageId, "");
      break;
    }
    resolve(message_to_pos)
    
  })

}

exports.parseRes = parseRes