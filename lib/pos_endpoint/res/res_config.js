const { getConfig } = require('../../connection/db');

const resConfig = async (resultArr) => {
  let {pos_config_type, vat_tax_rate, svc_tax_rate, state_tax_rate, state_tax_active, svc_active} = await getConfig()
  let vat_tax_value
  let state_tax_value
  let svc_tax_value
  let net_value
  let total_value

  let tenderString = resultArr[5]    
  let [checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal] = await parseTenders(tenderString)
  console.log([checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal])
  let payment_type = taxableTenders.length == 0 ? "0" :  taxableTenders[0][1]

  // 1. Inclusive, Exclusive (Vat,SVC,LCT) SVC as TAX
  if(pos_config_type==1){
    vat_tax_value = parseFloat(resultArr[6])
    state_tax_value = parseFloat(resultArr[7])
    svc_tax_value = parseFloat(resultArr[8])
    total_value = taxableTenderTotal
    net_value = taxableTenderTotal - (vat_tax_value + svc_tax_value + state_tax_value)

    if (noTaxTenders.length !== 0){
      let mypercentage = (taxableTenderTotal/checkTendersTotal) * 100
      vat_tax_value =  (mypercentage/100) * vat_tax_value
      state_tax_value = (mypercentage/100) * state_tax_value
      svc_tax_value = (mypercentage/100) * svc_tax_value
      net_value = (mypercentage/100) * net_value
    }

    return [ vat_tax_value, state_tax_value, svc_tax_value, net_value, checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal, payment_type]
  }

  //2. Exclusive(Vat,SVC,LCT) SVC as Auto Service Charge
  if(pos_config_type==2){
    vat_tax_value = parseFloat(resultArr[6])
    state_tax_value = parseFloat(resultArr[7])
    svc_tax_value = parseFloat(resultArr[12]) + parseFloat(resultArr[13])
    total_value = checkTendersTotal
    net_value = checkTendersTotal - (vat_tax_value + svc_tax_value + state_tax_value)

    if (noTaxTenders.length !== 0){
      let mypercentage = (taxableTenderTotal/checkTendersTotal) * 100
      vat_tax_value =  (mypercentage/100) * vat_tax_value
      state_tax_value = (mypercentage/100) * state_tax_value
      svc_tax_value = (mypercentage/100) * svc_tax_value
      net_value = (mypercentage/100) * net_value
    }

    return [ vat_tax_value, state_tax_value, svc_tax_value, net_value, checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal, payment_type]
  
  }

  //3. Combined Taxes for Transcorp Calabar
  if(pos_config_type==3){
    let combined_tax_value = parseFloat(resultArr[6])
    let combined_tax_rate = parseFloat(vat_tax_rate) + parseFloat(state_tax_rate) + parseFloat(svc_tax_rate)
    total_value = checkTendersTotal
    net_value = taxableTenderTotal - combined_tax_value
    vat_tax_value = parseFloat( ((vat_tax_rate/(100 + combined_tax_rate)) * (total_value)).toFixed(2) )
    state_tax_value = state_tax_active ? parseFloat( ((state_tax_rate/(100 + combined_tax_rate)) * (total_value)).toFixed(2)    ) : 0.0
    svc_tax_value = svc_active ? parseFloat( ((svc_tax_rate/(100 + combined_tax_rate)) * (total_value)).toFixed(2)  )  : 0.0

    if (noTaxTenders.length !== 0){
      let mypercentage = (taxableTenderTotal/checkTendersTotal) * 100
      vat_tax_value =  (mypercentage/100) * vat_tax_value
      state_tax_value = (mypercentage/100) * state_tax_value
      svc_tax_value = (mypercentage/100) * svc_tax_value
      net_value = (mypercentage/100) * net_value
    }
    return [ vat_tax_value, state_tax_value, svc_tax_value, net_value, checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal, payment_type]
  }

}

async function parseTenders(tenderString){
  let {pos_not_taxable} = await getConfig()
  pos_not_taxable = pos_not_taxable.split(",")
  let reducer = (a, c) => a + c
  let checkTenders = tenderString
    .split("||") //Split tender string by || character
    .filter(e => e) // remove blanks
    .map(obj => obj.split("|-") ) // map through and split again by |-
    //.filter(obj => !pos_not_taxable.includes(obj[1])) // filter only taxable tenders
     
  let checkTendersTotal = checkTenders.map(obj => {
    let priceField = obj[3]
    let result = priceField.endsWith("-") ? parseFloat(-(priceField.slice(0,-1))) : parseFloat(priceField)
    return result
  } ).reduce(reducer,0.0)

  let noTaxTenders = checkTenders.filter(obj => pos_not_taxable.includes(obj[1]))
  let taxableTenders = checkTenders.filter(obj => !pos_not_taxable.includes(obj[1]))
  
  let taxableTenderTotal = taxableTenders.map(obj => {
    let priceField = obj[3]
    let result = priceField.endsWith("-") ? parseFloat(-(priceField.slice(0,-1))) : parseFloat(priceField)
    return result
  } ).reduce(reducer,0.0)

  return [checkTendersTotal, taxableTenders, noTaxTenders, taxableTenderTotal]
}

exports.resConfig = resConfig