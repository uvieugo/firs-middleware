const needle = require('needle');
const {getAuthToken, getConfig} = require('../connection/db')
const logger = require('../logger')

const postToFirs = async (message) => {
  
  let access_token = await getAuthToken()
  let {tax_base_url} = await getConfig()
  return new Promise(resolve => {
    let postFormat = JSON.stringify(message)
    let options = {
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(postFormat),
        'Authorization': `Bearer ${access_token}`
      },
      open_timeout: 10000,
      rejectUnauthorized: false
    };

    needle.post(tax_base_url, message, options, (err, resp) => {

      logger.log({level: 'info', 'label': 'TOFIRS', message: JSON.stringify(message) })

      let security_code = message.bill.security_code
      let vat_id = message.bill.vat_number
      let bill_number = message.bill.bill_number
      let office_id = message.bill.business_place
      let device_number = message.bill.business_device
      let date_time = message.bill.bill_datetime
      let total = message.bill.total_value
      let result
      if (err) {
        console.log(err)
        logger.log({level: 'error', 'label': 'FIRSPOSTERROR', message: JSON.stringify(err) })
        result = {
          error: err,
          security_code: security_code,
          vat_id: vat_id,
          bill_number: bill_number,
          office_id: office_id,
          device_number: device_number,
          date_time: date_time,
          total: total

        }
      } else {
        console.log(resp.body)
        if(resp.statusCode==200){
          // console.log(resp.body)
          logger.log({level: 'info', 'label': 'FIRSPOSTRESPONSE', message: JSON.stringify(resp.body) })
          result = {
            status_code: resp.statusCode,
            payment_code: resp.body.payment_code,
            security_code: security_code,
            created_at: resp.body.created_at
          }
        }else{
          // console.log(resp.body)
          logger.log({level: 'info', 'label': 'FIRSPOSTRESPONSE', message: JSON.stringify(resp.body) })
          // console.log(resp.body)
          result = {
            error: {code: resp.statusCode},
            error_message: resp.body,
            security_code: security_code,
            vat_id: vat_id,
            bill_number: bill_number,
            office_id: office_id,
            device_number: device_number,
            date_time: date_time,
            total: total
          }
        }
      }
      // logger.info(result)
      logger.log({level: 'info', 'label': 'FROMFIRS', message: JSON.stringify(result) })
      resolve(result)
    });

  });

}

exports.postToFirs = postToFirs