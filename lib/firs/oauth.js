const needle = require('needle');
const moment = require('moment');
const fs = require('fs');
const {getAuthData, updateAuth, getConfig} = require("../connection/db");
const logger = require('../logger')

const getAuthToken = async () => {
	const {
		grant_type,
		auth_base_url,
		auth_username,
		password,
		client_id,
		client_secret,
	} = await getConfig()

	return new Promise(resolve => {

		let auth_data = {
			'grant_type': grant_type,
			'username': auth_username,
			'password': password,
			'client_id': client_id,
			'client_secret': client_secret
		}

		let options = {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			rejectUnauthorized: false
		}

		needle.post(auth_base_url, auth_data, options, (err, resp) => {
			if (err){
				console.table(err)
				logger.log({level: 'error', 'label': 'Get Auth Error', message: JSON.stringify(err) })
				resolve('error')
			}else{
				if (resp.statusCode==200){
					logger.log({level: 'info', label: 'Get Auth Token', message: `Auth Token: ${resp.body.access_token}`})
					// console.log(`Auth Token: ${resp.body.access_token}`)
					// let now = new Date()
					// let auth_expiry_date = new Date(now.getTime() + resp.body.expires_in*1000).toISOString();
					let now = moment().format()
					let auth_expiry_date = moment( moment(now).valueOf() + resp.body.expires_in*1000).format()
					console.log(now, auth_expiry_date)
					updateAuth(resp.body,now,auth_expiry_date)
					// updateAuth(resp.body,now.toISOString(),auth_expiry_date)
					resolve('ok')
				}else{
					logger.log({level: 'error', 'label': 'Get Auth Token Error', message: JSON.stringify(resp.body)})
					resolve('error')
				}
			}
		})
	});
}

const checkTokenExpired = async () => {
	console.log("checking token")
	let authData = await getAuthData()

	if ( authData.expiry_date == null || authData == "" ) {
		getAuthToken()
	}

	let auth_expiry_date = authData.expiry_date
	let time_now = moment().format()

	if (auth_expiry_date < time_now){
		getAuthToken()
	}

}

exports.getAuthToken = getAuthToken
exports.checkTokenExpired = checkTokenExpired