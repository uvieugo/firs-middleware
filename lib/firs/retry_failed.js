const { getFailedMessages, getFailedMessages_manual, updateMessage, getMessage } = require("../connection/db");
const {postToFirs} = require('./firs_post');

const retryFailedMessages = async (source,startDate,endDate) =>{
  let messages = await getFailedMessages_manual(source,startDate,endDate)
  
  messages.forEach((message,index) => {
    let newMessage = JSON.parse(message.message)
    let rowID = JSON.parse(message.id)
    newMessage.bill.resend = 1
    // postToFirs(JSON.stringify(newMessage)).then( (response ) => {
    //   // console.log(response)
    //   if (response.hasOwnProperty('error')){
    //     if(response.error.code==400){
    //       updateMessage(rowID,"",response.error.code,"error"); 
    //     }else{
    //       updateMessage(rowID,"",response.error.code,"error");
    //     }
    //   }else{
    //     updateMessage(rowID,response.payment_code,response.status_code,"success"); 
    //   }
    // });
    setTimeout(async () => {
      let response = await postToFirs(JSON.stringify(newMessage))
      if (response.hasOwnProperty('error')){
        if(response.error.code==400){
          updateMessage(rowID,{ response_code: response.error.code, success: false}); 
        }else{
          updateMessage(rowID,{ response_code: response.error.code, success: false}); 
        }
      }else{
        updateMessage(rowID,{ firs_response: response.payment_code, response_code: response.error.code, success: true}); 
      }
      console.log(response)
      
    }, index*5000);
    
  });
}

const retryFailedMessages_auto = async () =>{
  let messages = await getFailedMessages()
  // console.log(messages)
  messages.forEach((message,index) => {
    let newMessage = JSON.parse(message.message)
    let rowID = JSON.parse(message.id)
    newMessage.bill.resend = 1
    setTimeout(async () => {
      let response = await postToFirs(newMessage)
      console.log(newMessage)
      console.log(typeof newMessage)
      if (response.hasOwnProperty('error')){
        if(response.error.code==400){
          updateMessage(rowID,{ response_code: response.error.code, success: false}); 
        }else{
          updateMessage(rowID,{ response_code: response.error.code, success: false}); 
        }
      }else{
        updateMessage(rowID,{ firs_response: response.payment_code, response_code: response.status_code, success: true}); 
      }
      console.log(response)
      
    }, index*5000);
    
  });
}

const retryFailedMessage = async (id) =>{
  let message = await getMessage(id)
  let newMessage = JSON.parse(message.message_to_firs)
  let rowID = JSON.parse(message.id)
    newMessage.bill.resend = 1
  postToFirs(JSON.stringify(newMessage)).then( (response ) => {
    console.log(response)
    if (response.hasOwnProperty('error')){
      updateMessage(rowID,{ response_code: response.error.code, success: false, error_response: JSON.stringify(response.error_message)}); 
      // updateMessage(rowID,"",response.error.code,"error",JSON.stringify(response.error_message)); 
      // if(response.error.code==400){
      // }else{
      //   updateMessage(rowID,"",response.error.code,"error");
      // }
    }else{
      // updateMessage(rowID,response.payment_code,response.status_code,"success"); 
      updateMessage(rowID,{ firs_response: response.payment_code, response_code: response.error.code, success: true}); 
    }
  });
    
}

exports.retryFailedMessages = retryFailedMessages
exports.retryFailedMessages_auto = retryFailedMessages_auto
exports.retryFailedMessage = retryFailedMessage
