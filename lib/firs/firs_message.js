const md5 = require('md5');
const {getDeviceMap, getPaymentMap, getConfig} = require ('../connection/db')

const generateFirsMessage = async (message,source) => {
  const {client_secret,vat_number,business_place,pms_business_device,pos_business_device} = await getConfig()
  const firs_cash = await getPaymentMap("C")
  const firs_bank_t = await getPaymentMap("T")
  const firs_debit_c = await getPaymentMap("D")
  const firs_credit_c = await getPaymentMap("K")
  const firs_post_pay = await getPaymentMap("P")
  const firs_other = await getPaymentMap("O")

 
  function setPaymentType(value) {
    let result
    if (firs_cash.includes(value)){
      result = 'C'
    }
    else if ( firs_bank_t.includes(value) ){
      result = 'T'
    }
    else if ( firs_debit_c.includes(value) ){
      result = 'D'
    }
    else if ( firs_credit_c.includes(value) ){
      result = 'K'
    }
    else if ( firs_post_pay.includes(value) ){
      result = 'P'
    }
    else if ( firs_other.includes(value) ){
      result = 'O'
    }
    else{
      result = 'O'
    } 
    return result
  }

  let bill_number
  let bill_datetime
  let total_value
  let tax_free = "0.00"
  let base_value
  let bill_taxes
  let bill_tax_gst
  let bill_tax_other
  let business_device
  let payment_type = "C";

  if (source == 'PMS'){

    business_device = pms_business_device
    bill_number = message.bill_number
    bill_datetime = message.bill_datetime
    total_value = parseFloat(message.total_value).toFixed(2)
    tax_free = parseFloat(message.tax_free).toFixed(2)
    base_value = parseFloat(message.base_value).toFixed(2)
    bill_taxes = message.bill_taxes
    bill_tax_gst = message.bill_tax_gst
    bill_tax_other = message.bill_tax_other
    payment_type = setPaymentType(message.payment_type.toString())
 
  }
  else if(source == 'LEGACYPMS'){
    business_device = pms_business_device
    bill_number = message.bill_number
    bill_datetime = message.bill_datetime
    total_value = parseFloat(message.total_value).toFixed(2)
    base_value = parseFloat(message.net_value).toFixed(2)
    bill_taxes = message.bill_taxes
    bill_tax_gst = message.bill_tax_gst
    bill_tax_other = message.bill_tax_other

  }
  else{

    let getDevice  = await getDeviceMap(message.business_device)
    if( getDevice !==null ){ business_device = getDevice}
    else{business_device = pos_business_device}
    bill_number = message.bill_number
    bill_datetime = message.bill_datetime
    total_value = parseFloat(message.total_value).toFixed(2)
    tax_free = parseFloat(message.tax_free).toFixed(2)
    base_value = parseFloat(message.base_value).toFixed(2)
    bill_taxes = message.bill_taxes
    bill_tax_gst = message.bill_tax_gst
    bill_tax_other = message.bill_tax_other
    payment_type = setPaymentType(message.payment_type.toString())
    
  }
  
  let security_code = md5(client_secret+vat_number+business_place+business_device+bill_number+bill_datetime+total_value);

  let post_data = {
    "bill": {
    "vat_number": vat_number,
    "business_place": business_place,
    "business_device": business_device,
    "bill_number": bill_number,
    "bill_datetime": bill_datetime,
    "total_value": total_value,
    "tax_free": tax_free,
    "payment_type": payment_type,
    "security_code": security_code
    },
    "bill_taxes": bill_taxes,
    "bill_tax_other": bill_tax_other,
    "bill_tax_gst": bill_tax_gst,
  }

  return post_data

}

exports.generateFirsMessage = generateFirsMessage