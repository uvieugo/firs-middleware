const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  pms_port: parseInt(process.env.PMS_PORT),
  pos_port: parseInt(process.env.POS_PORT),
  admin_port: parseInt(process.env.ADMIN_PORT), 
};

