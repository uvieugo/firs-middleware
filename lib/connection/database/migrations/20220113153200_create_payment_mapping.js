
exports.up = function(knex) {
  return knex.schema
    .createTable('payment_type_map', function (table) {
      table.increments('id');
      table.string('payment_id');
      table.string('firs_payment_code');
    })
  
};

exports.down = function(knex) {
  return knex.schema
      .dropTable("payment_type_map")

};
