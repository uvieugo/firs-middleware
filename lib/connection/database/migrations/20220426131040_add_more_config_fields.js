
exports.up = function(knex) {
  return knex.schema.table('config', function(table) {
    table.boolean('state_tax_active').defaultTo(0);
    table.boolean('svc_active').defaultTo(0);
    table.boolean('mask_guest_info').defaultTo(0);
    table.string('pos_vat_tax_string');
    table.string('pos_state_tax_string');
    table.string('pos_svc_tax_string');
    table.string('pms_vat_tax_string');
    table.string('pms_svc_tax_string');
    table.string('pms_state_tax_string');
    table.string('pms_pos_rooms');
    table.string('pos_not_taxable');
    table.boolean('pos_tax_combined').defaultTo(0);
    table.string('pos_tax_combined_string');
    table.string('vat_tax_rate');
    table.string('svc_tax_rate');
    table.string('state_tax_rate');
    table.string('pos_config_type');
    // table.string('pos_vat_tax_rate');
    // table.string('pos_state_tax_rate');
    // table.string('pos_svc_tax_rate');
  })
};

exports.down = function(knex) {
  return knex.schema.table('config', function(table) {
    table.dropColumn('state_tax_active');
    table.dropColumn('svc_active');
    table.dropColumn('mask_guest_info');
    table.dropColumn('pos_vat_tax_string');
    table.dropColumn('pos_state_tax_string');
    table.dropColumn('pos_svc_tax_string');
    table.dropColumn('pms_vat_tax_string');
    table.dropColumn('pms_svc_tax_string');
    table.dropColumn('pms_state_tax_string');
    table.dropColumn('pms_pos_rooms');
    table.dropColumn('pos_not_taxable');
    table.dropColumn('pos_tax_combined');
    table.dropColumn('pos_tax_combined_string');
    table.dropColumn('vat_tax_rate');
    table.dropColumn('svc_tax_rate');
    table.dropColumn('state_tax_rate');
    table.dropColumn('pos_config_type');
    // table.dropColumn('pos_vat_tax_rate');
    // table.dropColumn('pos_state_tax_rate');
    // table.dropColumn('pos_svc_tax_rate');
  })
};
