
exports.up = function(knex) {
  return knex.schema.table('authorisation', function(table) {
    table.date('received_date');
    table.date('expiry_date');
  })
};

exports.down = function(knex) {
 return knex.schema.table('authorisation', function(table) {
    table.dropColumn('expiry_date');
    table.dropColumn('received_date');
  })
};
