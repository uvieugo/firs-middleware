
exports.up = function(knex) {
  return knex.schema.table('config', function(table) {
    table.renameColumn('business_device', 'pms_business_device')
    table.string('pos_business_device');
  })
};

exports.down = function(knex) {
  return knex.schema.table('config', function(table) {
    table.dropColumn('pos_business_device');
    table.renameColumn('pms_business_device', 'business_device')
  })
};
