
exports.up = function(knex) {
  return knex.schema.table('messages', function(table) {
    table.date('error_response');
  })
};

exports.down = function(knex) {
  return knex.schema.table('messages', function(table) {
    table.dropColumn('error_response');
  })
};
