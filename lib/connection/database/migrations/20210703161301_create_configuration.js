
exports.up = function(knex) {
  return knex.schema
    .createTable('config', function (table) {
      table.increments('id');
      table.string('grant_type');
      table.string('auth_username');
      table.string('password');
      table.string('client_id');
      table.string('client_secret');
      table.string('vat_number');
      table.string('business_place');
      table.string('business_device');
      table.string('tax_base_url');
      table.string('auth_base_url');
     
    })
    .createTable('messages', function (table) {
       table.increments('id');
       table.text('fiscal_message');
       table.string('source')
       table.text('message_to_firs');
       table.text('firs_response');
       table.text('response_code');
       table.boolean('success');
       table.timestamps(true,true);
    })
    .createTable('authorisation',function(table){
      table.increments('id');
      table.string('access_token');
      table.integer('expires_in');
      table.string('token_type');
      table.string('scope');
      table.string('refresh_token');
    })
    .createTable('checks', function(table){
      table.increments('id');
      table.string('checknum');
      table.float('checktotal');
    })
    .createTable('devices', function(table){
      table.increments('id');
      table.string('workstation_id');
      table.string('business_device');
      table.string('device_type');
      table.unique('workstation_id');
    })
    
};

exports.down = function(knex) {
  return knex.schema
      .dropTable("config")
      .dropTable("messages")
      .dropTable("authorisation")
      .dropTable("checks")
      .dropTable("devices");
};
