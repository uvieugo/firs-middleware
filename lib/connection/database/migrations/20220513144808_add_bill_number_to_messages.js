
exports.up = function(knex) {
  return knex.schema.table('messages', function(table) {
    table.text('system_bill_number');
    table.text('void_status');
    table.text('parsed_message');
  })
};

exports.down = function(knex) {
  return knex.schema.table('messages', function(table) {
    table.dropColumn('system_bill_number');
    table.dropColumn('void_status');
    table.dropColumn('parsed_message');
  })
};