exports.seed = function(knex, Promise) {
  knex('authorisation').count('id as count')
  .then(function(result){
    let count = result[0].count
    if( count == 0){
      //  Inserts seed entries
    return knex('authorisation').insert({
      access_token: "",
      expires_in: null,
      token_type: "",
      scope: null,
      refresh_token: "",
      received_date: null,
      expiry_date: null
    }).then( () => {})
    }

  })

  knex('config').count('id as count')
  .then(function(result){
    let count = result[0].count
    if( count == 0){
      //  Inserts seed entries
    return knex('config').insert({
      auth_username: "",
      password: "",
      client_id: null,
      client_secret: "",
      vat_number: null,
      business_place: null
    }).then( () => {})
    }

  })

};