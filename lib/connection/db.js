const dotenv = require('dotenv');
dotenv.config();
const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const secretKey = process.env.DB_SECRET
const iv = Buffer.from('720ebe3ba2841a589a78fcfffdae6840', 'hex');
const knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: './lib/connection/database/db.sqlite'
  },
  migrations: {
    directory: './lib/connection/database/migrations',
    tableName: 'knex_migrations'
  },
  seeds: {
    directory: './lib/connection/database/seeds'
  },
  useNullAsDefault: true,
});

const dbMigrate = () => {
  return new Promise (resolve => {
    knex.migrate.latest()
    .then(() => {
      console.log('migration run')
        knex.seed.run();
        resolve('nil') 
      })
    });

}

const insertMessage = (message) => {
  message.fiscal_message = JSON.stringify(message.fiscal_message)
  return new Promise (resolve => {
    knex('messages').insert(message)
    .then( result => resolve(result[0]))
  });
}

const updateMessage = (rowID,message) => {
  return new Promise (resolve => {
    knex('messages')
    .where('id', rowID)
    .update(message)
    .then( result => resolve(result))
  })
}

const getBillNumber = (system_bill_number,source) => {
  return new Promise (resolve => {
    knex('messages').where({system_bill_number: system_bill_number, source: source}).where({void_status: null}).first()
    .then(row => {
      row == undefined ? resolve(null) : resolve(row.id)
    })
  })
}

// const updateFirsMessage = (rowID,firsMessage) => {
//   return new Promise (resolve => {
//     knex('messages')
//     .where('id', rowID)
//     .update({
//       message_to_firs: firsMessage
//     }).then( result => resolve(result))
//   })
// }

// const updateData = (rowID,firsResponse,responseCode,updateType,error_message) => {
//   if(updateType=='success'){
//     knex('messages')
//     .where('id', rowID)
//     .update({
//       firs_response: firsResponse,
//       response_code: responseCode,
//       success: true
//     })
//     .then(result => console.log("Record Updated"));

//   }else{
//     knex('messages')
//     .where('id', rowID)
//     .update({
//       response_code: responseCode,
//       error_response: error_message,
//       success: false
//     })
//     .then(result => console.log("Record Updated"));
//     }
// }

const updateAuth = (resp,received_date,expiry_date) => {
  knex.table('authorisation').first("id")
  .then(function(row) { 
    // console.log(row); 
    let rowID = row.id
    let {access_token,expires_in,token_type,scope,refresh_token} = resp
    // let someDate = knex.fn.
    knex('authorisation')
    .where('id', rowID)
    .update({
        access_token: access_token,
        expires_in: expires_in,
        token_type: token_type,
        scope: scope,
        refresh_token: refresh_token,
        received_date: received_date,
        expiry_date: expiry_date
      })
      .then( () => {})
  });
}

const getAuthData = () => {
  return new Promise (resolve => {
    knex.table('authorisation').first()
    .then( row => resolve(row))
  })
}

const getAuthToken = () => {
  return new Promise (resolve => {
    knex.table('authorisation').first()
    .then( (row) => {
      // console.log(row.access_token)
      resolve(row.access_token)
    })

  })
}

const getFailedMessages = () => {
  return new Promise (resolve => {
    knex.table('messages')
    .whereNot({response_code: 200})
    // .whereNot({response_code: 400})
    .then( row => {
      let result = row.map(item => {
        return { id: item.id, message: item.message_to_firs}
      })
      resolve(result)
    })
  })
}

const getFailedMessages_manual = (source,start_date,end_date) => {
  return new Promise (resolve => {
    knex.table('messages')
    .where('created_at', '>=', start_date)
    .where('created_at', '<', end_date)
    .where('source', source)
    .whereNot({response_code: 200})
    .whereNot({response_code: 400})
    .where(function(){
      this.orWhere({success: null}).orWhere({success: false})
    })
    .then( row => {
      let result = row.map(item => {
        return { id: item.id, message: item.message_to_firs}
      })
      resolve(result)
    })
  })
}

const getFailedMessages_next = (source,start_date,end_date) => {
  return new Promise (resolve => {
    knex.table('messages')
    .where('created_at', '>=', start_date)
    .where('created_at', '<', end_date)
    .where('source', source)
    .where(function(){
      this.orWhere({success: null}).orWhere({success: false})
    })
    .then( rows => {
      let result = rows.map(row=> {
        return { }
      })
      resolve(rows)
    })
  })
}

// const insertCheck = (checkNumber) => {
//   return new Promise (resolve => {
//     knex('checks').insert({
//       checknum: checkNumber,
//       checktotal: 0
//     }).then( result => resolve(result))
//   });

// }

// const updateCheck = (checkNumber, tenderAmount) => {
//   knex('checks')
//   .where('checknum', checkNumber)
//   .increment({
//     checktotal: tenderAmount
//   })
//   .then(result => console.log("Record Updated"));
// }

// const getCheck = (checkNumber) => {
//   return new Promise (resolve => {
//     knex('checks').where('checknum', checkNumber)
//     .then(row => resolve(row))
//   })
// }

const insertDeviceMap = (workstation_id,business_device,device_type) => {
  return new Promise (resolve => {
    knex('devices').insert({
      workstation_id: workstation_id,
      business_device: business_device,
      device_type: device_type
    })
    .then( result => resolve(result))
    .catch( error => {
      resolve(error)
    })
  });
}

const getAllDeviceMap = () => {
  return new Promise (resolve => {
    knex.table('devices')
    .then( row => {
      let result = row.map(item => {
        return { id: item.id, workstation_id: item.workstation_id, business_device: item.business_device, device_type: item.device_type}
      })
      resolve(result)
    })
  })
}

const getDeviceMap = (workstation_id) => {
  return new Promise (resolve => {
    knex('devices').where('workstation_id', workstation_id).first()
    .then(row => resolve(row.business_device))
    .catch( error => {
      if (error) {resolve(null) }
    })
  })
}

const updateDeviceMap = (rowID) => {
  
}

const getPaymentMap = (pString) => {
  // console.log(pString)
  return new Promise ( resolve => {
    knex('payment_type_map').where('firs_payment_code', pString)
    .then( rows => {
      // console.log(rows)
      let result = rows.map( row => row.payment_id)
      resolve(result)
    })
  })
}

const viewMessages = (source,startDate,endDate) => {
  return new Promise (resolve => {
    let start_date = startDate == "" ? new Date() : startDate
    let end_date = endDate == "" ? new Date() : endDate
    console.log(start_date, end_date)
    knex('messages')
    .where('source', source)
    .where('created_at', '>=', start_date)
    .where('created_at', '<', end_date)
    .where('void_status', null)
    .whereNot('message_to_firs', "[object Object]").whereNot('message_to_firs', "")
    .then(rows => {
      let result = rows.map( row =>{
        let obj
        let billNumber
        let firsBillNo = JSON.parse(row.message_to_firs).bill.bill_number
        let totalValue = JSON.parse(row.message_to_firs).bill.total_value
        let vatValue = JSON.parse(row.message_to_firs).bill_taxes[0].value
        if (row.source == "PMS"){
          if ( JSON.parse(row.fiscal_message).hasOwnProperty('DocumentInfo') ){
            billNumber = JSON.parse(row.fiscal_message).DocumentInfo.BillNo == "" ? JSON.parse(row.fiscal_message).DocumentInfo.FiscalFolioId : JSON.parse(row.fiscal_message).DocumentInfo.BillNo
            let roomNumber = JSON.parse(row.fiscal_message).hasOwnProperty('ReservationInfo') ? JSON.parse(row.fiscal_message).ReservationInfo.RoomNumber : ""
            let firstName = JSON.parse(row.fiscal_message).hasOwnProperty('ReservationInfo') ? JSON.parse(row.fiscal_message).ReservationInfo.GuestInfo.FirstName : ""
            let lastName = JSON.parse(row.fiscal_message).hasOwnProperty('ReservationInfo') ? JSON.parse(row.fiscal_message).ReservationInfo.GuestInfo.LastName : ""
            obj = {
              source: row.source,
              first_name: firstName,
              last_name: lastName,
              room_number: roomNumber,
              folio_number: row.system_bill_number,
              firs_bill_no: firsBillNo,
              total: totalValue,
              vat: vatValue,
              date: row.created_at,
              void_stat: row.void_status
    
            }
          }else{
            let reducer = (a, c) => a + c
            let headData = JSON.parse(row.fiscal_message).head
            let documentInfo = headData.document_info
            let bodyData = JSON.parse(row.fiscal_message).body
            let generalInfo = headData.general
            let guestInfo = headData.guest_info
            let firstName = guestInfo['FIRSTNAME']
            let lastName = guestInfo ['LASTNAME']
            let roomNumber = generalInfo['ROOMNUMBER']
            let billNumber = documentInfo['BILLNUMBER']
            let total_info = bodyData.total_info
            let firsBillNo = row.id
            let totalValue = total_info.TOTALGROSSAMOUNT
            obj = {
              source: row.source,
              first_name: firstName,
              last_name: lastName,
              room_number: roomNumber,
              folio_number: row.system_bill_number,
              firs_bill_no: firsBillNo,
              total: totalValue,
              vat: vatValue,
              date: row.created_at,
              void_stat: row.void_status
          
            }
          }
        }
        else if (row.source == "POS"){
          billNumber = JSON.parse(row.fiscal_message).bill_number
          obj = {
              source: row.source,
              bill_number: billNumber,
              firs_bill_no: row.system_bill_number,
              total: totalValue,
              vat: vatValue,
              date: row.created_at,
              void_stat: row.void_status
    
            }
        }
        return obj
      })
      resolve(result);
    })
  })
}

const viewPMSMessages = (source,startDate,endDate) => {
  return new Promise (resolve => {
    let start_date = startDate == "" ? new Date() : startDate
    let end_date = endDate == "" ? new Date() : endDate
    console.log(start_date, end_date)
    knex('messages')
    .where('source', source)
    .where('created_at', '>=', start_date)
    .where('created_at', '<', end_date)
    .whereNot('message_to_firs', "[object Object]").whereNot('message_to_firs', "")
    .then(rows => {
      let result = rows.map( row =>{
        let billNumber
        let firsBillNo = JSON.parse(row.message_to_firs).bill.bill_number
        let totalValue = JSON.parse(row.message_to_firs).bill.total_value
        let vatValue = JSON.parse(row.message_to_firs).bill_taxes[0].value
        // console.log(JSON.parse(row.message_to_firs))
        // console.log(row.message_to_firs)
        if (row.source == "PMS"){
          // billNumber = JSON.parse(row.fiscal_message).DocumentInfo.BillNo
          billNumber = JSON.parse(row.fiscal_message).hasOwnProperty('ReservationInfo') ? JSON.parse(row.fiscal_message).ReservationInfo.RoomNumber : JSON.parse(row.fiscal_message).DocumentInfo.BillNo
        }
        else if (row.source == "POS"){
          billNumber = JSON.parse(row.fiscal_message).bill_number
        }
        else{
          billNumber = JSON.parse(row.fiscal_message).bill_number
        }
        let obj = {
          source: row.source,
          bill_number: billNumber,
          firs_bill_no: firsBillNo,
          total: totalValue,
          vat: vatValue,
          date: row.created_at,

        }
        return obj
      })
      resolve(result);
    })
  })
}

const getMessage = (id) => {
  return new Promise (resolve => {
    knex('messages').where("id", id).first()
    .then( row => resolve(row) )
  })
}

const getUser = (username, password, cb) => {
  return new Promise (resolve => {
    knex('users').where("username", username).first()
    .then( row => resolve(row) )
  })
}

const getConfig = () => {
  return new Promise (resolve => {
    knex.table('config').first()
    .then( row =>{
      row.client_secret=decrypt(row.client_secret)
      resolve(row)
    })
  }) 
}

const updateConfig = (configData) => {
  return new Promise (resolve => {
    knex.table('config').first()
    .then( row => {
      let rowID = row.id
      let {auth_username,password,client_id,client_secret,vat_number,business_place,grant_type,tax_base_url,auth_base_url,pms_business_device,pos_business_device,
        state_tax_active,svc_active,mask_guest_info,pos_config_type,pos_vat_tax_string,pos_state_tax_string,pos_svc_tax_string,pos_tax_combined,pos_tax_combined_string,
        pms_vat_tax_string,pms_svc_tax_string,pms_state_tax_string,vat_tax_rate,svc_tax_rate,state_tax_rate, pms_pos_rooms,pos_not_taxable
      } = configData
      knex('config')
      .where('id', rowID)
      .update({
        auth_username: auth_username,
        password: password,
        client_id: client_id,
        client_secret: encrypt(client_secret),
        vat_number: vat_number,
        business_place: business_place,
        grant_type: grant_type,
        auth_base_url: auth_base_url,
        tax_base_url, tax_base_url,
        pms_business_device: pms_business_device,
        pos_business_device: pos_business_device,
        state_tax_active: state_tax_active,
        svc_active: svc_active,
        mask_guest_info: mask_guest_info,
        pos_config_type: pos_config_type,
        pos_vat_tax_string: pos_vat_tax_string,
        pos_state_tax_string: pos_state_tax_string,
        pos_svc_tax_string: pos_svc_tax_string,
        pos_tax_combined: pos_tax_combined,
        pos_tax_combined_string: pos_tax_combined_string,
        // pos_vat_tax_rate: pos_vat_tax_rate,
        // pos_state_tax_rate: pos_state_tax_rate,
        // pos_svc_tax_rate: pos_svc_tax_rate,
        pms_vat_tax_string: pms_vat_tax_string,
        pms_svc_tax_string: pms_svc_tax_string,
        pms_state_tax_string: pms_state_tax_string,
        pms_pos_rooms: pms_pos_rooms,
        vat_tax_rate: vat_tax_rate,
        svc_tax_rate: svc_tax_rate,
        state_tax_rate: state_tax_rate,
        pos_not_taxable: pos_not_taxable
      })
      .then( () => resolve(getConfig()))

    })

  })
}

const encrypt = (text) => {
  const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
  return encrypted.toString('hex')
};

const decrypt = (hash) => {
  const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(iv, 'hex'));
  const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash, 'hex')), decipher.final()]);
  return decrpyted.toString();
};

exports.insertMessage = insertMessage
exports.updateMessage = updateMessage
exports.getBillNumber = getBillNumber
// exports.dbUpdate = updateData
// exports.updateFirs = updateFirsMessage
exports.updateAuth = updateAuth
exports.getAuthToken = getAuthToken
exports.getAuthData = getAuthData
exports.getFailedMessages = getFailedMessages
exports.getFailedMessages_next = getFailedMessages_next
exports.getFailedMessages_manual = getFailedMessages_manual 
exports.getMessage = getMessage
// exports.insertCheck = insertCheck
// exports.updateCheck = updateCheck
// exports.getCheck = getCheck
exports.dbMigrate = dbMigrate
exports.insertDeviceMap = insertDeviceMap
exports.getAllDeviceMap = getAllDeviceMap
exports.getDeviceMap = getDeviceMap
exports.updateDeviceMap = updateDeviceMap
exports.getPaymentMap = getPaymentMap
exports.viewPMSMessages = viewPMSMessages
exports.viewMessages = viewMessages
exports.getUser = getUser
exports.db = knex
exports.getConfig = getConfig
exports.updateConfig = updateConfig
