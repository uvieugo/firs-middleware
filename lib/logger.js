const winston = require('winston');
require('winston-daily-rotate-file');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const transport_error = new winston.transports.DailyRotateFile({
  filename: 'lib/log/error-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '100m',
  level: 'error',
  frequency: '24h'
});

const transport_combined = new winston.transports.DailyRotateFile({
  filename: 'lib/log/application-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  zippedArchive: true,
  maxSize: '100m',
  frequency: '24h'
});

const logger = winston.createLogger({
  level: 'info',
  format: combine(
    timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSSZZ' }),
    myFormat
  ),
  transports: [
    transport_error,
    transport_combined
  ],
});

module.exports = logger