// Update with your config settings.

module.exports = {

  development: {
    client: 'sqlite3',
    connection: {
      filename: './lib/connection/database/db.sqlite'
    },
    migrations: {
      directory: './lib/connection/database/migrations'
    },
    useNullAsDefault: true,
  },

  staging: {
    client: 'sqlite3',
    connection: {
      filename: './lib/connection/database/db.sqlite'
    },
    migrations: {
      directory: './lib/connection/database/migrations'
    },
    useNullAsDefault: true,
  },

  production: {
    client: 'sqlite3',
    connection: {
      filename: './lib/connection/database/db.sqlite'
    },
    migrations: {
      directory: './lib/connection/database/migrations'
    },
    useNullAsDefault: true,
  },

};
