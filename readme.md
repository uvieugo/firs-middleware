## FIRS Middleware for OperaPMS, RES3700, Oracle Simphony

## Requirements
+ Nodejs Version 12

## Installation instructions
+ Rename .env-sample to .env and add the relevant details
+ Navigate to the install folder
+ Run install.bat or install.exe to install as a windows service
+ Run start.bat or start.exe to start a temporary session